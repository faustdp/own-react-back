import express from 'express';
import { createServer } from 'http';
import { resolve } from 'path';
import cors from 'cors';
import helmet from 'helmet';
import session from 'express-session';
import connectRedis from 'connect-redis';
import Redis from 'ioredis';
import { ApolloServer } from 'apollo-server-express';
import { makeExecutableSchema } from '@graphql-tools/schema';
import ws from 'ws';
import { useServer } from 'graphql-ws/lib/use/ws';
import typeDefs from './graphql/typedefs';
import initResolvers, { addUserToChats } from './graphql/resolvers';
import pubsub, { USER_CONNECTED, USER_TYPING } from './pubsub';
import connectDb from './db/connectDb';
import { Chat } from './db/chat';
import config, { mainChatName, isProduction } from './config';
import { name, datatype } from 'faker';

(async () => {
  await connectDb(config.dbUrl);

  const mainChat = (await Chat.findOne({ room: mainChatName }, 'id')) || (await Chat.create({ room: mainChatName }));
  const mainId = mainChat.id;

  const RedisStore = connectRedis(session);
  const client = new Redis(config.redisUrl);
  const store = new RedisStore({
    client,
    disableTouch: true,
  });

  client.on('error', console.error);

  const sessionHandler = session({
    store,
    secret: config.secretKey,
    name: config.sessionName,
    resave: false,
    rolling: false,
    saveUninitialized: false,
    cookie: {
      httpOnly: true,
      sameSite: isProduction,
      secure: isProduction,
      maxAge: config.sessionMaxAge,
    },
  });

  const schema = makeExecutableSchema({ typeDefs, resolvers: initResolvers(mainId, store) });

  const apollo = new ApolloServer({
    schema,
    context: ({ req, res, connection }) => (connection ? connection.context : { req, res }),
  });

  await apollo.start();

  const app = express();

  if (isProduction) {
    app.get('*.js', (req, res, next) => {
      if (req.header('Accept-Encoding').includes('br')) {
        req.url = req.url + '.br';
        res.set('Content-Encoding', 'br');
        res.set('Content-Type', 'application/javascript; charset=UTF-8');
      }
      next();
    });

    app.get('*.css', (req, res, next) => {
      if (req.header('Accept-Encoding').includes('br')) {
        req.url = req.url + '.br';
        res.set('Content-Encoding', 'br');
        res.set('Content-Type', 'text/css');
      }
      next();
    });

    app.use(express.static('front/build'));
    app.set('trust proxy', 1);
  }

  const corsOptions = {
    origin: config.corsOrigin,
    credentials: true,
  };

  app.use(cors(corsOptions));

  app.use(express.json());

  app.use(express.urlencoded({ extended: true }));

  app.use(
    helmet({ contentSecurityPolicy: { useDefaults: true, directives: { 'img-src': ['"self"', 'blob:', 'data:'] } } }),
  );

  app.use(sessionHandler);

  app.use((req, res, next) => {
    if (!req.session.username) {
      req.session.username = `${name.firstName()}-${datatype.number()}`;
      req.session.userId = null;
      req.session.ids = [];
      req.session.rooms = [{ id: mainId, room: mainChatName }];
    } else if (req.method === 'GET') {
      req.session.cookie.expires = new Date(Date.now() + config.sessionMaxAge);
      req.session.save();
    }
    next();
  });

  app.use((err, _req, res, _next) => {
    if (!err.status) {
      console.error(err.stack);
    }
    res.status(err.status || 500).json({ message: err.message || 'Internal Server Error' });
  });

  if (isProduction) {
    app.get('*', (req, res) => {
      req.originalUrl.endsWith('.js') || req.originalUrl.endsWith('.css')
        ? res.redirect(301, '/')
        : res.sendFile(resolve('front/build/index.html'));
    });
  }

  apollo.applyMiddleware({ app, path: config.graphqlPath, cors: corsOptions });

  const server = createServer(app);

  await new Promise((resolve) => {
    server.listen(config.port, () => {
      const wsServer = new ws.Server({
        server,
        path: config.graphqlPath,
      });

      useServer(
        {
          schema,

          async onConnect({ connectionParams, extra: { request } }) {
            try {
              const { id, rooms: clientRooms } = connectionParams;

              const { session } = await new Promise((resolve) => sessionHandler(request, {}, () => resolve(request)));

              const { username, rooms, ids } = session;

              if (!username) {
                throw new Error('Session required');
              }

              if (!ids.includes(id)) {
                session.ids.push(id);
                await new Promise((resolve) => session.save(resolve));
              }
              const roomIds = clientRooms.length > 0 ? clientRooms : rooms.map((room) => room.id);
              await addUserToChats(roomIds, username, id);
            } catch (error) {
              throw new Error(error);
            }
          },

          async onDisconnect({ connectionParams, extra }) {
            try {
              const session = await new Promise((resolve, reject) =>
                store.load(extra.request.sessionID, (err, user) => {
                  if (err) reject(err);
                  resolve(user);
                }),
              );

              if (!connectionParams || !session) return;

              const { id } = connectionParams;

              const { username, rooms, ids } = session;
              const index = ids.indexOf(id);
              if (index !== -1) {
                session.ids.splice(index, 1);
                await new Promise((resolve) => session.save(resolve));
              }

              const roomIds = rooms.map((room) => room.id);
              const chats = await Chat.find({ _id: { $in: roomIds } });
              await Promise.all(
                chats.map((chat) => {
                  const userExists = chat.online.find((el) => el.username === username);
                  if (!userExists || !userExists.connections.includes(id)) return;
                  const lastConnection = userExists.connections.length <= 1;
                  const isTyping = userExists.typing.includes(id);
                  const promises = [];
                  const shouldPublish = chat.online.length > 1;

                  if (isTyping && (userExists.typing.length <= 1 || lastConnection) && shouldPublish) {
                    promises.push(
                      pubsub.publish(USER_TYPING, {
                        [USER_TYPING]: { username, typing: false, room: chat.id },
                      }),
                    );
                  }

                  if (lastConnection) {
                    promises.push(
                      chat.updateOne({ $pull: { online: { username } } }).exec(),
                      shouldPublish &&
                        pubsub.publish(USER_CONNECTED, {
                          [USER_CONNECTED]: { username, connected: false, room: chat.id },
                        }),
                    );
                    if (chat.id === mainId && shouldPublish) {
                      promises.push(
                        pubsub.publish(`${USER_CONNECTED}.${username}`, {
                          [USER_CONNECTED]: { username, connected: false },
                        }),
                      );
                    }
                  } else {
                    userExists.connections.splice(userExists.connections.indexOf(id), 1);
                    if (isTyping) userExists.typing.splice(userExists.typing.indexOf(id), 1);
                    promises.push(chat.save());
                  }
                  return promises;
                }),
              );
            } catch (error) {
              console.log(error);
            }
          },
        },
        wsServer,
      );

      resolve();
    });
  });
})();
