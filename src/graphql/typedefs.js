import { gql } from 'apollo-server-express';

export default gql`
  scalar Date

  type User {
    username: String!
    events(name: String): [Event]
    rooms: [Room]
  }

  type Event {
    name: String!
    date: Date!
    id: ID!
  }

  type Room {
    id: ID!
    room: String
    participant: String
    unreadCount: Int
  }

  type Message {
    username: String!
    date: Date!
    message: String!
    id: ID
    room: String
  }

  type MeResponse {
    username: String!
    isLogged: Boolean!
    rooms: [Room]
  }

  input EventInput {
    name: String!
    date: Date!
    id: ID!
  }

  type ChatUser {
    username: String!
    typing: Boolean
  }

  type ChatUserResponse {
    username: String!
    prevUsername: String
    room: String
    connected: Boolean
    typing: Boolean
  }

  type ChatResponse {
    messages: [Message]
    cursor: ID
  }

  type JoinRoomResponse {
    id: ID
    unreadCount: Int
  }

  type changeChatResponse {
    room: String!
    participant: String
    prevRoom: String
  }

  type Query {
    me: MeResponse
    chatQuery(room: String, cursor: ID, limit: Int): ChatResponse
    chatUsers(room: String, participant: String): [ChatUser]
    events: [Event]
    findRoom(room: String!, isPrivate: Boolean): [String]
  }

  type Mutation {
    signUp(username: String!, password: String!): String
    logIn(username: String!, password: String!): User
    logOut: MeResponse
    addEvent(events: [EventInput!]): Boolean
    removeEvent(id: ID!): Boolean
    addMessage(room: String!, date: Date!, message: String!, id: ID!, clientId: ID!, participant: String): Message
    userTyping(room: String!, clientId: ID!, typing: Boolean!): Boolean
    joinRoom(room: String!, leave: Boolean!, clientId: ID!, byRoomName: Boolean): JoinRoomResponse
    createRoom(clientId: ID!, room: String, participant: String): Room
    setUnreadChat(clientId: ID!, room: String, participant: String): Boolean
  }

  type Subscription {
    chatAdded(rooms: [String]!, clientId: ID!, username: String!): Message
    userConnected(username: String!, room: String, participant: String): ChatUserResponse
    userTypingSubscription(username: String!, room: String): ChatUserResponse
    changeChat(username: String!, clientId: ID!): changeChatResponse
  }

  schema {
    query: Query
    mutation: Mutation
    subscription: Subscription
  }
`;
