import { User, Event } from '../db/user';
import { Chat, Message } from '../db/chat';
import config, { mainChatName } from '../config';
import { GraphQLScalarType } from 'graphql';
import { Kind } from 'graphql/language';
import { withFilter } from 'graphql-subscriptions';
import { AuthenticationError, UserInputError } from 'apollo-server-express';
import pubsub, { CHAT_ADDED, USER_CONNECTED, USER_TYPING, CHANGE_CHAT } from '../pubsub';
import { name, datatype } from 'faker';

async function changeUser(session, username, store) {
  const { rooms, ids, username: prevUsername } = session;
  if (ids.length === 0) return;
  const roomIds = rooms.map((room) => room.id);
  const chats = await Chat.find({ _id: { $in: roomIds } });
  await Promise.all(
    chats.map((chat) => {
      const promises = [];
      const user = chat.online.find((el) => el.username === prevUsername);
      if (!user) return;
      const userExists = chat.online.find((el) => el.username === username);
      const matchedIds = [];
      const remainingIds = [];
      user.connections.forEach((id) => (ids.includes(id) ? matchedIds.push(id) : remainingIds.push(id)));
      const typingIds = user.typing.filter((id) => ids.includes(id));
      if (typingIds.length > 0) {
        if (typingIds.length < user.typing.length) {
          user.typing = user.typing.filter((id) => !ids.includes(id));
          if (!userExists || (userExists && userExists.typing.length === 0)) {
            promises.push(
              pubsub.publish(USER_TYPING, {
                [USER_TYPING]: { username, typing: true, room: chat.id },
              }),
            );
          }
        } else {
          if (!userExists || (userExists && userExists.typing.length === 0)) {
            promises.push(
              pubsub.publish(USER_TYPING, {
                [USER_TYPING]: { username, prevUsername, room: chat.id },
              }),
            );
          } else if (userExists && userExists.typing.length > 0) {
            promises.push(
              pubsub.publish(USER_TYPING, {
                [USER_TYPING]: { username: prevUsername, typing: false, room: chat.id },
              }),
            );
          }
        }
      }
      if (userExists) {
        userExists.connections.push(...matchedIds);
        userExists.typing.push(...typingIds);
        if (remainingIds.length === 0) {
          chat.online.splice(chat.online.indexOf(user), 1);
          promises.push(
            pubsub.publish(USER_CONNECTED, {
              [USER_CONNECTED]: { username: prevUsername, connected: false, room: chat.id },
            }),
            pubsub.publish(`${USER_CONNECTED}.${prevUsername}`, {
              [USER_CONNECTED]: { username, connected: false },
            }),
          );
        }
      } else {
        if (remainingIds.length === 0) {
          user.username = username;
          promises.push(
            pubsub.publish(USER_CONNECTED, {
              [USER_CONNECTED]: { username, prevUsername, room: chat.id },
            }),
            pubsub.publish(`${USER_CONNECTED}.${prevUsername}`, {
              [USER_CONNECTED]: { username, connected: false },
            }),
            pubsub.publish(`${USER_CONNECTED}.${username}`, {
              [USER_CONNECTED]: { username, connected: true },
            }),
          );
        } else {
          chat.online.push({
            username,
            connections: matchedIds,
            typing: typingIds,
          });
          promises.push(
            pubsub.publish(USER_CONNECTED, {
              [USER_CONNECTED]: { username, connected: true, room: chat.id },
            }),
            pubsub.publish(`${USER_CONNECTED}.${username}`, {
              [USER_CONNECTED]: { username, connected: true },
            }),
          );
        }
      }
      if (remainingIds.length !== 0) {
        user.connections = remainingIds;
      }
      promises.push(chat.save());
      return Promise.all(promises);
    }),
  );

  const createdChats = await Chat.find({ $or: [{ createdBy: prevUsername }, { participants: prevUsername }] });
  if (createdChats.length) {
    await Promise.all(
      createdChats.map(async (chat) => {
        if (chat.createdBy) {
          chat.createdBy = username;
          return chat.save();
        } else {
          const otherParticipant = chat.participants[chat.participants[0] === prevUsername ? 1 : 0];
          const chatExists = await Chat.findOne({
            participants: { $all: [username, otherParticipant] },
            private: true,
          });
          store.all((err, sessions) => {
            if (err) return;
            const users = sessions.filter((sess) => sess.username === otherParticipant);
            if (users.length === 0) return;
            users.forEach((user) => {
              const room = user.rooms.find((el) => el.participant === prevUsername);
              const nextRoomInd = user.rooms.findIndex((el) => el.participant === username);
              if (room) {
                room.participant = username;
                if (chatExists) {
                  room.id = chatExists.id;
                  if (nextRoomInd >= 0) {
                    user.rooms.splice(nextRoomInd, 1);
                  }
                }
                const { id, ...sess } = user;
                store.set(id, sess, (err) => err && console.log(err));
              }
            });
          });
          if (chatExists) {
            const roomExists = rooms.find((el) => el.id === chat.id);
            if (roomExists) {
              roomExists.id = chatExists.id;
              session.save();
            }
            if (chat.unreadMessages > 0) {
              chatExists.unreadMessages = chat.unreadMessages;
            }
            return Promise.all([
              pubsub.publish(`${CHANGE_CHAT}.${otherParticipant}`, {
                [CHANGE_CHAT]: { room: chatExists.id, participant: username, prevRoom: chat.id },
              }),
              pubsub.publish(`${CHANGE_CHAT}.${prevUsername}`, {
                [CHANGE_CHAT]: { room: chatExists.id, participant: otherParticipant, prevRoom: chat.id },
              }),
              Message.updateMany({ room: chat._id }, [
                {
                  $set: {
                    room: chatExists._id,
                    username: {
                      $cond: [{ $eq: ['$username', prevUsername] }, username, '$username'],
                    },
                  },
                },
              ]).exec(),
              Chat.deleteOne({ _id: chat._id }),
              chat.unreadMessages > 0 && chatExists.save(),
            ]);
          } else {
            chat.participants.splice(chat.participants.indexOf(prevUsername), 1, username);
            return Promise.all([
              chat.save(),
              Message.updateMany({ room: chat._id }, [
                {
                  $set: {
                    username: {
                      $cond: [{ $eq: ['$username', prevUsername] }, username, '$username'],
                    },
                  },
                },
              ]).exec(),
              pubsub.publish(`${CHANGE_CHAT}.${otherParticipant}`, {
                [CHANGE_CHAT]: { room: chat.id, participant: username },
              }),
              pubsub.publish(`${CHANGE_CHAT}.${prevUsername}`, {
                [CHANGE_CHAT]: { room: chat.id, participant: otherParticipant },
              }),
            ]);
          }
        }
      }),
    );
  }
}

async function joinChat(chat, clientId, username, rooms, roomId, user, session) {
  if (user) {
    user.connections.push(clientId);
    await chat.save();
  } else {
    const shouldPublish = chat.online.length > 0;
    await Promise.all([
      chat.updateOne({ $push: { online: { username, connections: [clientId], typing: [] } } }).exec(),
      shouldPublish &&
        pubsub.publish(USER_CONNECTED, {
          [USER_CONNECTED]: { username, room: roomId, connected: true },
        }),
    ]);
    if (!rooms.some((el) => el.id === roomId)) {
      rooms.push({
        id: roomId,
        ...(chat.room
          ? { room: chat.room }
          : { participant: chat.participants[chat.participants[0] === username ? 1 : 0] }),
      });
      session.save();
    }
  }
}

async function addUserToChats(roomIds, username, ids) {
  const chats = await Chat.find({ _id: { $in: roomIds } });
  await Promise.all(
    chats.map((chat) => {
      const userExists = chat.online.find((el) => el.username === username);
      if (userExists) {
        Array.isArray(ids) ? userExists.connections.push(...ids) : userExists.connections.push(ids);
        return chat.save();
      } else {
        const shouldPublish = chat.online.length > 0;
        return Promise.all([
          chat
            .updateOne({
              $push: { online: { username, connections: Array.isArray(ids) ? [...ids] : [ids], typing: [] } },
            })
            .exec(),
          shouldPublish &&
            pubsub.publish(USER_CONNECTED, {
              [USER_CONNECTED]: { username, connected: true, room: chat.id },
            }),
          shouldPublish &&
            chat.room === mainChatName &&
            pubsub.publish(`${USER_CONNECTED}.${username}`, {
              [USER_CONNECTED]: { username, connected: true },
            }),
        ]);
      }
    }),
  );
}

async function getUnreadRooms(rooms, username, prevUsername) {
  const privateRooms = rooms.reduce((acc, curr) => (curr.participant && acc.push(curr.id), acc), []);
  if (privateRooms.length === 0) return rooms;

  const unreadChats = await Chat.find(
    { _id: { $in: privateRooms }, private: true, unreadMessages: { $gt: 0 } },
    'unreadMessages participants',
  );
  if (unreadChats.length === 0) return rooms;

  const unreadMessages = await Promise.all(
    unreadChats.map((el) => Message.find({ room: el.id }, 'room username').sort('-_id').limit(1).exec()),
  );
  const unreadMessagesFlat = unreadMessages.flat();
  const unreadRooms = rooms.map((el) => ({ ...el }));
  unreadMessagesFlat.forEach((msg) => {
    const room = msg.room.toString();
    const foundRoom = unreadRooms.find((el) => el.id === room);
    const chat = unreadChats.find((el) => el.id === room);
    if (foundRoom) {
      foundRoom.unreadCount =
        msg.username === username || (prevUsername && msg.username === prevUsername) ? 0 : chat.unreadMessages;
    }
  });

  return unreadRooms;
}

function initResolvers(mainId, store) {
  const resolvers = {
    Date: new GraphQLScalarType({
      name: 'Date',
      description: 'Date type',
      parseValue(value) {
        return new Date(value);
      },
      serialize(value) {
        return value;
      },
      parseLiteral(ast) {
        if (ast.kind !== Kind.STRING) {
          throw new Error(`Can only parse strings to dates but got a ${ast.kind}`);
        }
        return new Date(ast.value);
      },
    }),

    Query: {
      async me(obj, args, { req: { session } }) {
        const { username, rooms, userId } = session;
        if (!username) throw new AuthenticationError('Couldn"t find user');

        const unreadRooms = await getUnreadRooms(rooms, username);

        return { username, rooms: unreadRooms, isLogged: !!userId };
      },

      async chatQuery(obj, { cursor, limit, room }) {
        if (!room) {
          room = mainId;
        }
        const query = cursor ? { room, _id: { $lt: cursor } } : { room };
        const chats = await Message.find(query).lean().sort('-_id').limit(limit).exec();
        const messages = chats.reverse();
        const nextCursor = messages.length === limit ? messages[0]._id : null;
        return {
          messages,
          cursor: nextCursor,
        };
      },

      async chatUsers(obj, { room, participant }) {
        const query = room && !participant ? { _id: room } : { room: mainChatName };
        const [chat, privateChat] = await Promise.all([
          Chat.findOne(query),
          !!participant && !!room && Chat.findOne({ _id: room }),
        ]);
        if (!chat) throw new Error('Couldn"t find chat');
        let chatUsers;
        if (participant) {
          const user = chat.online.find((el) => el.username === participant);
          const isTyping = privateChat?.online?.find((el) => el.username === participant)?.typing?.length > 0;
          chatUsers = user
            ? [
                {
                  username: user.username,
                  typing: isTyping,
                },
              ]
            : [];
        } else {
          chatUsers = chat.online.reduce((total, curr) => {
            total.push({
              username: curr.username,
              typing: !!curr.typing.length,
            });
            return total;
          }, []);
        }
        return chatUsers;
      },

      async events(obj, params, { req }) {
        const events = await Event.find({ userId: req.session.userId }, 'name date id -_id').exec();
        return events;
      },

      async findRoom(obj, { room, isPrivate }, { req: { session } }) {
        if (isPrivate) {
          const chat = await Chat.findOne(
            { participants: { $all: [room, session.username] }, private: true },
            '_id',
          ).lean();
          return chat ? [chat._id.toString()] : null;
        } else {
          const fixedRoom = room.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
          const reg = new RegExp(fixedRoom, 'i');
          const chats = await Chat.find({ room: { $regex: reg, $ne: mainChatName }, private: false })
            .lean()
            .limit(5)
            .exec();
          const rooms = chats.map((el) => el.room);
          return rooms;
        }
      },
    },

    Mutation: {
      async signUp(obj, { username, password }, { req: { session } }) {
        const userExist = await User.findOne({ username });
        if (userExist) {
          throw new UserInputError('Such user already exist');
        }
        const newUser = await User.create({ username, password, rooms: session.rooms.map((room) => room.id) });
        await changeUser(session, username, store);
        session.username = username;
        session.userId = newUser._id;
        session.save();
        return newUser.username;
      },

      async logIn(obj, { username, password }, { req: { session } }) {
        const user = await User.findOne({ username }).populate('rooms', 'id room participants').exec();
        if (!user) {
          throw new UserInputError('Wrong username');
        }
        const match = await user.comparePassword(password);
        if (!match) {
          throw new UserInputError('Wrong password');
        }
        const prevUsername = session.username;
        await changeUser(session, username, store);
        session.username = username;
        session.userId = user._id;
        const sessionIds = session.rooms.map((el) => el.id);
        const filteredRooms = user.rooms.filter((el) => !sessionIds.includes(el.id)).map((room) => room.toObject());
        filteredRooms.forEach((room) => {
          if (room.participants) {
            room.participant = room.participants[room.participants[0] === username ? 1 : 0];
            delete room.participants;
          }
        });
        session.rooms.push(...filteredRooms);
        session.save();
        const filteredIds = filteredRooms.map((el) => el.id);
        sessionIds.forEach((id) => {
          if (!user.rooms.some((el) => el.id === id)) {
            user.rooms.push(id);
          }
        });
        const [events, unreadRooms] = await Promise.all([
          Event.find({ userId: user._id }).lean(),
          getUnreadRooms(session.rooms, session.username, prevUsername),
          session.ids.length > 0 && addUserToChats(filteredIds, session.username, session.ids),
          user.save(),
        ]);
        return { username: session.username, rooms: unreadRooms, events };
      },

      async logOut(obj, params, { req, res }) {
        const { username, rooms } = req.session;
        const ids = [...req.session.ids];
        await new Promise((resolve, reject) => {
          req.session.regenerate((err) => {
            if (err) reject(err);
            res.clearCookie(config.sessionName);
            resolve();
          });
        });
        const newName = `${name.firstName()}-${datatype.number()}`;
        req.session.username = newName;
        req.session.userId = null;
        req.session.ids = ids;
        req.session.rooms = [{ id: mainId, room: mainChatName }];
        req.session.save();
        const roomIds = rooms.map((room) => room.id);
        const chats = await Chat.find({ _id: { $in: roomIds } });
        await Promise.all(
          chats.map((chat) => {
            const userExists = chat.online.find((el) => el.username === username);
            if (!userExists) return;
            const isTyping = userExists.typing.length > 0;
            userExists.typing = userExists.typing.filter((el) => !ids.includes(el));
            userExists.connections = userExists.connections.filter((el) => !ids.includes(el));
            const promises = [];
            const shouldPublish = chat.online.length > 1;
            if (isTyping && userExists.typing.length === 0 && shouldPublish) {
              promises.push(
                pubsub.publish(USER_TYPING, {
                  [USER_TYPING]: { username, typing: false, room: chat.id },
                }),
              );
            }
            if (userExists.connections.length === 0) {
              chat.online.splice(chat.online.indexOf(userExists), 1);
              shouldPublish &&
                promises.push(
                  pubsub.publish(chat.private ? `${USER_CONNECTED}.${username}` : USER_CONNECTED, {
                    [USER_CONNECTED]: { username, connected: false, ...(!chat.private && { room: chat.id }) },
                  }),
                );
            }
            promises.push(chat.save());
            return promises;
          }),
        );

        return { username: req.session.username, rooms: req.session.rooms, isLogged: false };
      },

      async addMessage(obj, { room = mainId, date, message, id, clientId, participant }, { req: { session } }) {
        const msg = { date, message, room, id, username: session.username };
        const [createdMsg] = await Promise.all([
          Message.create(msg),
          participant && Chat.updateOne({ _id: room }, { $inc: { unreadMessages: 1 } }),
          pubsub.publish(`${CHAT_ADDED}.${participant ? participant : room}`, { [CHAT_ADDED]: { ...msg, clientId } }),
          participant &&
            pubsub.subsRefsMap[`${CHAT_ADDED}.${session.username}`]?.length > 1 &&
            pubsub.publish(`${CHAT_ADDED}.${session.username}`, { [CHAT_ADDED]: { ...msg, clientId } }),
        ]);
        if (participant && !pubsub.subsRefsMap[`${CHAT_ADDED}.${participant}`]) {
          store.all((err, sessions) => {
            if (err) return;
            const users = sessions.filter((sess) => sess.username === participant);
            if (users.length === 0) return;
            users.forEach((user) => {
              if (!user.rooms.some((el) => el.id === room)) {
                user.rooms.push({ id: room, participant: session.username });
                const { id, ...sess } = user;
                store.set(id, sess, (err) => err && console.log(err));
              }
            });
          });
        }
        return createdMsg;
      },

      async addEvent(obj, { events }, { req }) {
        if (!req.session.userId) throw new AuthenticationError('Not authenticated');
        events.forEach((evt) => (evt.userId = req.session.userId));
        await Event.create(events);
        return true;
      },

      async removeEvent(obj, { id }) {
        await Event.deleteOne({ id });
        return true;
      },

      async userTyping(obj, { room = mainId, clientId, typing }, { req: { session } }) {
        const chat = await Chat.findOne({ _id: room });
        const user = chat.online.find((el) => el.username === session.username);
        if (!user) return null;
        const hasId = user.typing.includes(clientId);
        if (typing && !hasId) {
          user.typing.push(clientId);
        } else if (!typing && hasId) {
          user.typing.splice(user.typing.indexOf(clientId), 1);
        }
        const shouldPublish =
          chat.online.length > 1 && ((typing && user.typing.length === 1) || (!typing && user.typing.length === 0));
        await Promise.all([
          chat.save(),
          shouldPublish && pubsub.publish(USER_TYPING, { [USER_TYPING]: { typing, room, username: session.username } }),
        ]);
        return true;
      },

      async joinRoom(obj, { leave, room, clientId, byRoomName }, { req: { session } }) {
        const { username, userId, rooms } = session;
        const query = byRoomName ? { room, private: false } : { _id: room };
        const [chat, registeredUser] = await Promise.all([Chat.findOne(query), userId && User.findOne({ username })]);

        if (!chat) {
          const index = rooms.findIndex((el) => el.id === room);
          if (index !== -1) {
            rooms.splice(index, 1);
            session.save();
          }
          if (registeredUser) {
            const roomInd = registeredUser.rooms.indexOf(room);
            if (roomInd !== -1) {
              registeredUser.rooms.splice(roomInd, 1);
              await registeredUser.save();
            }
          }
          return null;
        }

        const user = chat.online.find((el) => el.username === username);
        const roomId = byRoomName ? chat.id : room;
        const result = { id: roomId };

        if (leave) {
          const roomInd = rooms.findIndex((el) => el.id === roomId);
          if (roomInd !== -1 && (!user || user.connections.length <= 1)) {
            rooms.splice(roomInd, 1);
            session.save();
          }
          if (!user) {
            return result;
          }
          if (user.connections.length > 1) {
            user.connections.splice(user.connections.indexOf(clientId), 1);
            await chat.save();
          } else {
            registeredUser && registeredUser.rooms.splice(registeredUser.rooms.indexOf(chat.id), 1);
            const shouldPublish = chat.online.length > 1;
            await Promise.all([
              chat.updateOne({ $pull: { online: { username } } }).exec(),
              shouldPublish &&
                pubsub.publish(USER_CONNECTED, {
                  [USER_CONNECTED]: { username, room: roomId, connected: false },
                }),
              registeredUser && registeredUser.save(),
            ]);
          }
        } else {
          await joinChat(chat, clientId, username, rooms, roomId, user, session);
          if (userId && !registeredUser.rooms.includes(chat.id)) {
            registeredUser.rooms.push(chat.id);
            await registeredUser.save();
          }
          if (!byRoomName && chat.unreadMessages > 0) {
            const msg = await Message.find({ room: roomId }, 'username').lean().sort('-_id').limit(1).exec();
            result.unreadCount = msg[0].username === username ? 0 : chat.unreadMessages;
          }
        }

        return result;
      },

      async createRoom(obj, { room, participant, clientId }, { req: { session } }) {
        const { username, userId, rooms } = session;
        const isPrivate = !!participant;
        if (!isPrivate) {
          const createdRooms = await Chat.find({ createdBy: username, private: false }).lean();
          if (createdRooms.length >= 3) {
            throw new Error('You сan"t create more than 3 public rooms');
          }
        }

        let chat;
        const chatQuery = isPrivate
          ? { participants: { $all: [participant, username] }, private: true }
          : { room: new RegExp(`^${room}$`, 'i'), private: false };
        chat = await Chat.findOne(chatQuery);

        if (chat) {
          const user = chat.online.find((el) => el.username === username);
          await joinChat(chat, clientId, username, rooms, chat.id, user, session);
        } else {
          const query = {
            private: isPrivate ? true : false,
            online: [{ username, connections: [clientId], typing: [] }],
            ...(!isPrivate && { createdBy: username }),
            ...(!isPrivate && { room }),
            ...(isPrivate && { participants: [username, participant] }),
          };
          chat = await Chat.create(query);
          rooms.push({
            id: chat.id,
            ...(chat.room
              ? { room: chat.room }
              : { participant: chat.participants[chat.participants[0] === username ? 1 : 0] }),
          });
          session.save();
        }

        if (userId) {
          const registeredUser = await User.findOne({ username });
          if (!registeredUser.rooms.includes(chat.id)) {
            registeredUser.rooms.push(chat.id);
            await registeredUser.save();
          }
        }

        return {
          id: chat.id,
          ...(chat.room
            ? { room: chat.room }
            : { participant: chat.participants[chat.participants[0] === username ? 1 : 0] }),
        };
      },

      async setUnreadChat(obj, { room, participant, clientId }, { req: { session } }) {
        await Promise.all([
          Chat.updateOne({ _id: room }, { unreadMessages: 0 }),
          pubsub.publish(`${CHANGE_CHAT}.${participant}`, {
            [CHANGE_CHAT]: { room, clientId },
          }),
          pubsub.subsRefsMap[`${CHANGE_CHAT}.${session.username}`]?.length > 1 &&
            pubsub.publish(`${CHANGE_CHAT}.${session.username}`, {
              [CHANGE_CHAT]: { room, clientId },
            }),
        ]);
        return true;
      },
    },

    Subscription: {
      chatAdded: {
        subscribe: withFilter(
          (_, args) => {
            const topics =
              args.rooms.length > 0 ? args.rooms.map((room) => `${CHAT_ADDED}.${room}`) : [`${CHAT_ADDED}.${mainId}`];
            topics.push(`${CHAT_ADDED}.${args.username}`);
            return pubsub.asyncIterator(topics);
          },
          (payload, variables) => payload.chatAdded.clientId !== variables.clientId,
        ),
      },
      userConnected: {
        subscribe: withFilter(
          (_, args) =>
            pubsub.asyncIterator(args.participant ? `${USER_CONNECTED}.${args.participant}` : USER_CONNECTED),
          (payload, variables) =>
            payload.userConnected.username !== variables.username &&
            payload.userConnected.prevUsername !== variables.username &&
            ((variables.room || mainId) === payload.userConnected.room ||
              variables.participant === payload.userConnected.username),
        ),
      },
      userTypingSubscription: {
        subscribe: withFilter(
          () => pubsub.asyncIterator(USER_TYPING),
          (payload, variables) =>
            (variables.room || mainId) === payload.userTypingSubscription.room &&
            payload.userTypingSubscription.username !== variables.username,
        ),
      },
      changeChat: {
        subscribe: withFilter(
          (_, args) => pubsub.asyncIterator(`${CHANGE_CHAT}.${args.username}`),
          (payload, variables) => payload.changeChat.clientId !== variables.clientId,
        ),
      },
    },
  };
  return resolvers;
}

export { addUserToChats };

export default initResolvers;
