import dotenv from 'dotenv';

dotenv.config();

const HOUR = 1000 * 60 * 60;
const WEEK = HOUR * 24 * 7;

const isProduction = process.env.NODE_ENV === 'production';

const config = {
  port: +process.env.PORT || 8000,
  graphqlPath: process.env.GRAPHQL_PATH,
  dbUrl: isProduction ? process.env.DB_URL : 'mongodb://localhost/react',
  redisUrl: isProduction ? process.env.REDIS_URL : 'redis://127.0.0.1:6379',
  corsOrigin: isProduction ? true : 'http://localhost:8080',
  saltRound: +process.env.SALT_ROUNDS,
  secretKey: process.env.SECRET_KEY,
  sessionName: process.env.SESSION_NAME,
  sessionMaxAge: WEEK,
  redisSecondUrl: !isProduction
    ? 'redis://127.0.0.1:6379'
    : process.env.REDIS_SECOND_URL
    ? process.env.REDIS_SECOND_URL
    : process.env.REDIS_URL,
};

const mainChatName = 'Main';

export default config;

export { mainChatName, isProduction };
