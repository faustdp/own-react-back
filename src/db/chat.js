import mongoose, { Schema } from 'mongoose';
import { mainChatName } from '../config';

const ChatSchema = Schema(
  {
    room: {
      type: String,
      required: function () {
        return !this.private;
      },
      minlength: [3, 'Room should be at least 3 characters long'],
    },
    online: [
      {
        username: String,
        connections: [String],
        typing: [String],
        _id: false,
      },
    ],
    private: {
      type: Boolean,
      required: true,
      default: false,
    },
    participants: {
      type: Array,
      required: function () {
        return this.private;
      },
      default: void 0,
    },
    unreadMessages: {
      type: Number,
      default: void 0,
    },
    createdBy: {
      type: String,
      required: function () {
        return !this.private && this.room !== mainChatName;
      },
    },
  },
  { toObject: { getters: true }, toJSON: { getters: true } },
);

ChatSchema.set('toObject', {
  transform: (doc, { __v, _id, ...rest }) => ({ ...rest, id: _id.toString() }),
});

ChatSchema.set('toJSON', {
  transform: (doc, { __v, _id, ...rest }) => ({ ...rest, id: _id.toString() }),
});

const Chat = mongoose.model('Chat', ChatSchema);

// ChatSchema.pre('save', function (next) {
//   if (this.private || !this.expireAt) return next();
//   const expire = new Date();
//   expire.setDate(expire.getDate() + 2);
//   this.expireAt = expire;
//   next();
// });
// ChatSchema.index({ expireAt: 1 }, { expireAfterSeconds: 0, background: false });

// async function removeMessages(e) {
//   if (e.operationType !== 'delete') return;
//   const {
//     documentKey: { _id },
//   } = e;
//   try {
//     await Promise.all([
//       Message.deleteMany({ room: _id }),
//       User.updateMany({}, { $pull: { rooms: _id } }, { multi: true }),
//     ]);
//   } catch (error) {
//     console.error(error);
//   }
// }
// Chat.watch().on('change', removeMessages);

const MessageSchema = Schema({
  username: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    required: true,
    default: Date.now,
  },
  message: {
    type: String,
    required: true,
  },
  room: {
    type: 'ObjectId',
    ref: 'Chat',
    required: true,
  },
  id: {
    type: String,
    unique: true,
  },
});

MessageSchema.post('save', function (doc, next) {
  if (!this.id) {
    this.id = doc._id;
    doc.save();
  }
  next();
});

const Message = mongoose.model('Message', MessageSchema);

export { Message, Chat };
