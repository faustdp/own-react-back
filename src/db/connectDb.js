import mongoose from 'mongoose';

async function connectDb(url) {
  const db = mongoose.connection;

  db.on('connected', () => console.log('db connected'));

  db.on('disconnected', () => console.log('db disconnected'));

  try {
    await mongoose.connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    return { db, mongoose };
  } catch (err) {
    console.log(`db error: ${err}`);
  }
}

export default connectDb;
