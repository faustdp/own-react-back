import mongoose, { Schema } from 'mongoose';
import { genSalt, hash, compare } from 'bcrypt';
import config from '../config';

const UserSchema = Schema({
  username: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    minlength: [4, 'Password is too short'],
  },
  rooms: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Chat',
    },
  ],
});

UserSchema.virtual('id').get(function () {
  return this._id.toString();
});

UserSchema.pre('save', function (next) {
  if (this.password && (this.isNew || this.isModified('password'))) {
    genSalt(config.saltRounds, (err, salt) => {
      if (err) return next(err);
      hash(this.password, salt, (err, hash) => {
        if (err) return next(err);
        this.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});

UserSchema.methods.comparePassword = async function (pw) {
  const match = await compare(pw, this.password);
  return match;
};

UserSchema.set('toObject', {
  transform: (doc, { __v, password, ...rest }) => rest,
});

const EventSchema = Schema({
  name: String,
  date: Date,
  id: String,
  expireAt: {
    type: Date,
    default: Date.now,
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
});

EventSchema.index({ expireAt: 1 }, { expireAfterSeconds: 0, background: false });

EventSchema.pre('save', function (next) {
  const expire = new Date(this.date);
  expire.setDate(expire.getDate() + 1);
  this.expireAt = expire;
  next();
});

const Event = mongoose.model('Event', EventSchema);
const User = mongoose.model('User', UserSchema);

export { Event, User };
