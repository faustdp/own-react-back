import { RedisPubSub } from 'graphql-redis-subscriptions';
import Redis from 'ioredis';
import config from './config';

const publisher = new Redis(config.redisSecondUrl);
const subscriber = new Redis(config.redisSecondUrl);

publisher.on('error', console.error);
subscriber.on('error', console.error);

const pubsub = new RedisPubSub({
  publisher,
  subscriber,
});

const CHAT_ADDED = 'chatAdded';
const USER_CONNECTED = 'userConnected';
const USER_TYPING = 'userTypingSubscription';
const CHANGE_CHAT = 'changeChat';

export { pubsub as default, CHAT_ADDED, USER_CONNECTED, USER_TYPING, CHANGE_CHAT };
