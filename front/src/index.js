import './helpers/polyfill';
import ReactDOM from 'react-dom';
import { Provider as ReduxProvider } from 'react-redux';
import store from './redux/store';
import { ApolloProvider } from '@apollo/client';
import client from './api/client';
import { BrowserRouter } from 'react-router-dom';
import App from 'App';
import { isProduction } from './config';
import './index.scss';

console.clear();

function Main() {
  return (
    <ApolloProvider client={client}>
      <ReduxProvider store={store}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </ReduxProvider>
    </ApolloProvider>
  );
}

const rootEl = document.getElementById('root');

ReactDOM.render(<Main />, rootEl);

if (!isProduction) {
  module.hot.accept();
}

if (isProduction) {
  window.addEventListener('load', () => {
    navigator.serviceWorker
      .register('/service-worker.js')
      .then((registration) => console.log('SW registered: ', registration))
      .catch((registrationError) => console.log('SW registration failed: ', registrationError));
  });
}
