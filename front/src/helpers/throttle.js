export default function throttle(func, ms, leading = true, trailing = true) {
  let timer = null,
    next = 0,
    nextArgs;
  function throttled(...args) {
    nextArgs = args;
    if (timer) return;
    const now = Date.now();
    const timeDiff = next - now;
    if (leading && timeDiff <= 0) {
      next = now + ms;
      return func.call(this, ...nextArgs);
    }
    if (trailing) {
      timer = setTimeout(
        () => {
          func.call(this, ...nextArgs);
          next = Date.now() + ms;
          timer = null;
        },
        leading ? timeDiff : ms,
      );
    }
  }
  throttled.cancel = function () {
    clearTimeout(timer);
    timer = null;
  };
  return throttled;
}
