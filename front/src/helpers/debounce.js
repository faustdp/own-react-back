export default function debounce(func, ms, leading = true, trailing = true) {
  let timer = null,
    last = 0;
  function debounced(...args) {
    const now = Date.now();
    const timePassed = now - last >= ms;
    if (!timePassed) {
      if (timer) clearTimeout(timer);
      if (trailing) {
        timer = setTimeout(() => {
          func.call(this, ...args);
          last = Date.now();
        }, ms);
      }
    } else if (leading) {
      func.call(this, ...args);
    }
    last = now;
  }
  debounced.cancel = function () {
    clearTimeout(timer);
    timer = null;
  };
  return debounced;
}
