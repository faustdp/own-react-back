/* eslint-disable max-len */
export const emoticonsReg =
  /:(?:'(?:-[()DO[\]o{}]|[()DO[\]o{}])|\{>|-[#$&(-*/3<>-@B-EJLOPSXZ-\]cjlopsxz-}]|[#$&(-*/3<>-@B-EJLOPSXZ-\]cjlopsxz|}][^\S\r\n])|=(?:'(?:-[()DO[\]o{}]|[()DO[\]o{}])|\{>|-[#$&(-*/3<>-@B-EJLOPSXZ-\]cjlopsxz-}]|[#$&(-*/3<>-@B-EJLOPSXZ-\]cjlopsxz|}])|[Oo][:=](?:-[)\]}]|[)\]}])|>(?:[:=](?:-[()/[-\]{}]|[()/[-\]{}])|0(?:-[)\]}]|[)\]}]))|%(?:-[([{]|[([{])|\\[Mm]\/|D(?:-[:=Xx]|[:=Xx])|8(?:-[#)D\]}]|[#)D\]}])|;(?:-[)P\]p}]|[)P\]p}])|x(?:-[(D[op{]|[(D[op{])|X(?:-[(DOP[{]|[(DOP[{])|<\/?3|[:=]\{/;
export const colonsReg = /:[a-zA-Z0-9-_+]+:(?::skin-tone-[2-6]:)?/;
export const nativeReg = /\p{Extended_Pictographic}/;
export const dataImgReg = /data:image\/(?:png|jpe?g|webp);base64,(?:[A-Za-z0-9]|[+/])+={0,2}/;
export const urlReg =
  /(?:https?:\/\/.)?(?:www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b(?:[-a-zA-Z0-9@:%_+.~#?&//=]*)/;
