function animate(params) {
  const { duration = 300, timing = (val) => val, cb = () => {} } = params;
  let { draw = () => {}, paused = false, reversed = false } = params;
  let start = performance.now();
  let prevProgress = reversed ? 1 : 0;
  let currProgress = 0;
  let timeScale = 1;
  let id = null;

  function anim(time) {
    if (paused) return;
    const diff = Math.max(time - start, 1);
    const progress = (diff / duration) * timeScale;
    currProgress = reversed ? prevProgress - progress : prevProgress + progress;
    if (currProgress > 1) currProgress = 1;
    if (currProgress < 0) currProgress = 0;
    const timeFraction = timing(currProgress);
    draw(timeFraction);
    if (currProgress === 1 || currProgress === 0) {
      stopAnim(true);
      cb(anim);
      return;
    }
    id = requestAnimationFrame(anim);
  }

  function stopAnim(isFinished) {
    paused = true;
    prevProgress = isFinished ? (reversed || timeScale < 0 ? 1 : 0) : currProgress;
    cancelAnimationFrame(id);
    return anim;
  }

  function startAnim(isPaused = false) {
    if (timeScale === 0) return anim;
    if (reversed && currProgress === 1 && timeScale < 0) prevProgress = 0;
    paused = isPaused;
    start = performance.now();
    id = requestAnimationFrame(anim);
    return anim;
  }

  anim.play = () => (paused ? startAnim() : anim);

  anim.pause = () => (!paused ? stopAnim() : anim);

  anim.toggle = () => (paused ? startAnim() : stopAnim());

  anim.restart = (func, isPaused) => {
    stopAnim();
    if (func) {
      draw = func;
    }
    timeScale = 1;
    reversed = params.reversed ?? false;
    prevProgress = reversed ? 1 : 0;
    return startAnim(isPaused ?? params.paused ?? false);
  };

  anim.paused = () => paused;

  anim.reversed = () => (reversed && timeScale >= 0) || (!reversed && timeScale < 0);

  anim.reverse = () => {
    reversed = !reversed;
    stopAnim();
    return startAnim();
  };

  anim.timeScale = (val) => {
    if (val === undefined) return timeScale;
    timeScale = +val;
    if (timeScale === 0) {
      return stopAnim();
    } else if (!paused) {
      start = performance.now();
      prevProgress = currProgress;
      return anim;
    }
  };

  id = requestAnimationFrame(anim);

  return anim;
}

const s = 1.70158;
const t = s * 1.525;

const Quadratic = {
  In: (k) => k * k,
  Out: (k) => k * (2 - k),
  InOut: (k) => ((k *= 2) < 1 ? 0.5 * k * k : -0.5 * (--k * (k - 2) - 1)),
};
const Cubic = {
  In: (k) => k * k * k,
  Out: (k) => --k * k * k + 1,
  InOut: (k) => ((k *= 2) < 1 ? 0.5 * k * k * k : 0.5 * ((k -= 2) * k * k + 2)),
};
const Quartic = {
  In: (k) => k * k * k * k,
  Out: (k) => 1 - --k * k * k * k,
  InOut: (k) => ((k *= 2) < 1 ? 0.5 * k * k * k * k : -0.5 * ((k -= 2) * k * k * k - 2)),
};
const Quintic = {
  In: (k) => k * k * k * k * k,
  Out: (k) => --k * k * k * k * k + 1,
  InOut: (k) => ((k *= 2) < 1 ? 0.5 * k * k * k * k * k : 0.5 * ((k -= 2) * k * k * k * k + 2)),
};
const Sinusoidal = {
  In: (k) => 1 - Math.cos((k * Math.PI) / 2),
  Out: (k) => Math.sin((k * Math.PI) / 2),
  InOut: (k) => 0.5 * (1 - Math.cos(Math.PI * k)),
};
const Exponential = {
  In: (k) => (k === 0 ? 0 : Math.pow(1024, k - 1)),
  Out: (k) => (k === 1 ? 1 : 1 - Math.pow(2, -10 * k)),
  InOut: (k) =>
    k === 0 ? 0 : k === 1 ? 1 : (k *= 2) < 1 ? 0.5 * Math.pow(1024, k - 1) : 0.5 * (-Math.pow(2, -10 * (k - 1)) + 2),
};
const Circular = {
  In: (k) => 1 - Math.sqrt(1 - k * k),
  Out: (k) => Math.sqrt(1 - --k * k),
  InOut: (k) => ((k *= 2) < 1 ? -0.5 * (Math.sqrt(1 - k * k) - 1) : 0.5 * (Math.sqrt(1 - (k -= 2) * k) + 1)),
};
const Elastic = {
  In: (k) => (k === 0 ? 0 : k === 1 ? 1 : -Math.pow(2, 10 * (k - 1)) * Math.sin((k - 1.1) * 5 * Math.PI)),
  Out: (k) => (k === 0 ? 0 : k === 1 ? 1 : Math.pow(2, -10 * k) * Math.sin((k - 0.1) * 5 * Math.PI) + 1),
  InOut: (k) =>
    k === 0 || k === 1
      ? k
      : ((k *= 2),
        k < 1
          ? -0.5 * Math.pow(2, 10 * (k - 1)) * Math.sin((k - 1.1) * 5 * Math.PI)
          : 0.5 * Math.pow(2, -10 * (k - 1)) * Math.sin((k - 1.1) * 5 * Math.PI) + 1),
};
const Back = {
  In: (k) => k * k * ((s + 1) * k - s),
  Out: (k) => --k * k * ((s + 1) * k + s) + 1,
  InOut: (k) => ((k *= 2) < 1 ? 0.5 * (k * k * ((t + 1) * k - t)) : 0.5 * ((k -= 2) * k * ((t + 1) * k + t) + 2)),
};
const Bounce = {
  In(k) {
    return 1 - this.Out(1 - k);
  },
  Out: (k) =>
    k < 1 / 2.75
      ? 7.5625 * k * k
      : k < 2 / 2.75
      ? 7.5625 * (k -= 1.5 / 2.75) * k + 0.75
      : k < 2.5 / 2.75
      ? 7.5625 * (k -= 2.25 / 2.75) * k + 0.9375
      : 7.5625 * (k -= 2.625 / 2.75) * k + 0.984375,
  InOut(k) {
    return k < 0.5 ? this.In(k * 2) * 0.5 : this.Out(k * 2 - 1) * 0.5 + 0.5;
  },
};
const Ease = {
  In: (k) => Math.pow(k, 1.675),
  Out: (k) => 1 - Math.pow(1 - k, 1.675),
  InOut: (k) => 0.5 * (Math.sin((k - 0.5) * Math.PI) + 1),
};

export default animate;

export { Quadratic, Cubic, Quartic, Quintic, Sinusoidal, Exponential, Circular, Elastic, Back, Bounce, Ease };
