const msPerSecond = 1000;
const msPerMinute = msPerSecond * 60;
const msPerHour = msPerMinute * 60;
const msPerDay = msPerHour * 24;
const msPerMonth = msPerDay * 30;
const msPerYear = msPerDay * 365;
const rtf = new Intl.RelativeTimeFormat(navigator.language, { numeric: 'auto' });

export default function timeDifference(date) {
  const now = Date.now();
  const updated = new Date(date).getTime();
  const elapsed = now - updated;

  if (elapsed < msPerMinute) {
    return rtf.format(-Math.trunc(elapsed / msPerSecond), 'second');
  } else if (elapsed < msPerHour) {
    return rtf.format(-Math.trunc(elapsed / msPerMinute), 'minute');
  } else if (elapsed < msPerDay) {
    return rtf.format(-Math.trunc(elapsed / msPerHour), 'hour');
  } else if (elapsed < msPerMonth) {
    return rtf.format(-Math.trunc(elapsed / msPerDay), 'day');
  } else if (elapsed < msPerYear) {
    return rtf.format(-Math.trunc(elapsed / msPerMonth), 'month');
  } else {
    return rtf.format(-Math.trunc(elapsed / msPerYear), 'year');
  }
}
