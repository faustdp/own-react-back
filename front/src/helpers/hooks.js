import { useEffect, useRef, useState, useCallback } from 'react';
import throttle from './throttle';

function useIsMounted() {
  const ref = useRef(false);
  useEffect(() => {
    ref.current = true;
    return () => {
      ref.current = false;
    };
  }, []);
  return useCallback(() => ref.current, []);
}

function usePrevious(value, def = null) {
  const ref = useRef(def);

  useEffect(() => {
    ref.current = value;
  }, [value]);

  return ref.current;
}

function usePreviousDifferent(value, def = null) {
  const currRef = useRef(def);
  const prevRef = useRef(def);

  useEffect(() => {
    prevRef.current = currRef.current;
    currRef.current = value;
  }, [value]);

  return value === currRef.current ? prevRef.current : currRef.current;
}

function useTimeout(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function cb() {
      savedCallback.current();
    }
    if (delay !== null) {
      const id = setTimeout(cb, delay);
      return () => clearTimeout(id);
    }
  }, [delay]);
}

function useThrottle(value, delay, leading = true, trailing = true) {
  const [state, setState] = useState(value);
  const throttled = useRef(throttle(setState, delay, leading, trailing)).current;

  useEffect(() => {
    throttled(value);
    return () => throttled.cancel();
  }, [value, throttled]);

  return state;
}

function useInterval(callback, delay) {
  const savedCallback = useRef(() => {});
  const cancelInterval = useRef(() => {});

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    if (delay == null) return;

    const id = setInterval(savedCallback.current, delay);
    cancelInterval.current = () => clearInterval(id);

    return () => clearInterval(id);
  }, [delay]);

  return useCallback(() => cancelInterval.current(), []);
}

export { useIsMounted, usePrevious, usePreviousDifferent, useInterval, useTimeout, useThrottle };
