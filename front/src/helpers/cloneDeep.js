function cloneDeep(target, cache = new WeakMap()) {
  if (typeof target !== 'object' || target === null || target instanceof HTMLElement) return target;
  if (cache.has(target)) return cache.get(target);
  const type = Object.getPrototypeOf(target).constructor;
  switch (type) {
    case Date:
      return cache.set(target, new Date(+target)).get(target);
    case RegExp:
      return cache.set(target, new RegExp(target.source, target.flags)).get(target);
    case Map:
      return cache.set(target, new Map(Array.from(target, ([key, val]) => [key, cloneDeep(val, cache)]))).get(target);
    case Set:
      return cache.set(target, new Set(Array.from(target, (val) => cloneDeep(val, cache)))).get(target);
    case Array:
      return cache
        .set(
          target,
          Array.from(target, (val) => cloneDeep(val, cache)),
        )
        .get(target);
    case Object:
      return cache
        .set(
          target,
          Object.keys(target).reduce((acc, curr) => ((acc[curr] = cloneDeep(target[curr], cache)), acc), {}),
        )
        .get(target);
    default:
      return target;
  }
}

export default cloneDeep;
