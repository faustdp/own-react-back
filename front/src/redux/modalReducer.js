import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  visibility: false,
  signUp: false,
  canvas: false,
};

const modalSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    toggleModal(state, action) {
      return {
        signUp: action.payload.signUp ?? state.signUp,
        canvas: action.payload.canvas ?? state.canvas,
        visibility: action.payload.visibility,
      };
    },
    toggleSignUp(state, action) {
      return {
        ...state,
        signUp: action.payload,
      };
    },
  },
});

export const { toggleModal, toggleSignUp } = modalSlice.actions;

export default modalSlice.reducer;
