import { createSlice } from '@reduxjs/toolkit';

const now = new Date();

const initialState = {
  date: now,
  currentDate: now,
  selectedDate: null,
  updates: true,
};

const calendarSlice = createSlice({
  name: 'calendar',
  initialState,
  reducers: {
    changeDate(state, action) {
      return {
        ...state,
        date: action.payload,
      };
    },
    selectDate(state, action) {
      return {
        ...state,
        selectedDate: action.payload,
      };
    },
    setCurrentDate(state, action) {
      return {
        ...state,
        currentDate: action.payload,
      };
    },
    updateView(state, action) {
      return {
        ...state,
        updates: action.payload,
      };
    },
  },
});

export const { changeDate, selectDate, setCurrentDate, updateView } = calendarSlice.actions;

export default calendarSlice.reducer;
