import { createSlice } from '@reduxjs/toolkit';
import { shallowEqual } from 'react-redux';
import { mainChatName, LS_EVENTS_NAME } from '../config';
import cloneDeep from '../helpers/cloneDeep';

const events = localStorage.getItem(LS_EVENTS_NAME);

const initialState = {
  logged: false,
  username: '',
  events: events ? JSON.parse(events) : [],
  rooms: [{ room: mainChatName, id: null }],
  unreadRooms: {},
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser(state, action) {
      return {
        ...state,
        username: action.payload.username,
        logged: action.payload.logged,
        ...(action.payload.rooms &&
          !shallowEqual(action.payload.rooms, state.rooms) && { rooms: action.payload.rooms }),
        ...(action.payload.unreadRooms &&
          !shallowEqual(action.payload.unreadRooms, state.unreadRooms) && {
            unreadRooms: action.payload.unreadRooms,
          }),
      };
    },
    logoutUser() {
      return initialState;
    },
    addEvent(state, action) {
      return {
        ...state,
        events: [...state.events, ...action.payload],
      };
    },
    removeEvent(state, action) {
      return {
        ...state,
        events: state.events.filter((event) => event.id !== action.payload),
      };
    },
    changeRooms(state, action) {
      let rooms = cloneDeep(state.rooms);
      if (action.payload.change) {
        const name = action.payload.room.participant ? 'participant' : 'room';
        const ind = rooms.findIndex((el) => el[name] === action.payload.room[name]);
        if (ind >= 0) {
          rooms[ind].id = action.payload.room.id;
        } else {
          rooms.push({ id: action.payload.room.id, [name]: action.payload.room[name] });
        }
      } else if (action.payload.leave) {
        const name = action.payload.room.id ? 'id' : action.payload.room.participant ? 'participant' : 'room';
        rooms = rooms.filter((el) => el[name] !== action.payload.room[name]);
      } else if (action.payload.rename) {
        const ind = rooms.findIndex((el) => el.id === action.payload.room.id);
        rooms[ind].participant = action.payload.room.participant;
      } else {
        const roomExists = rooms.find((room) => room.id === action.payload.room.id);
        if (roomExists) {
          const name = roomExists.participant ? 'participant' : 'room';
          roomExists[name] = action.payload.room[name];
        } else {
          rooms.push(action.payload.room);
        }
      }
      return {
        ...state,
        rooms,
      };
    },
    setUnreadRoom(state, action) {
      if (!(action.payload.room in state.unreadRooms) && action.payload.unreadCount == null) return state;
      const unreadRooms = { ...state.unreadRooms };
      if (action.payload.unreadCount == null) {
        delete unreadRooms[action.payload.room];
      } else {
        unreadRooms[action.payload.room] = action.payload.unreadCount;
      }
      return {
        ...state,
        unreadRooms,
      };
    },
  },
});

export const { setUser, logoutUser, addEvent, removeEvent, changeRooms, setUnreadRoom } = userSlice.actions;

export default userSlice.reducer;
