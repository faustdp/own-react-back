import { combineReducers } from '@reduxjs/toolkit';
import modalReducer from './modalReducer';
import calendarReducer from './calendarReducer';
import userReducer from './userReducer';

const rootReducer = combineReducers({ modal: modalReducer, calendar: calendarReducer, user: userReducer });

export default rootReducer;
