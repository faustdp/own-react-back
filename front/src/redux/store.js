import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './rootReducer';
import { LS_ACTION_NAME, LS_EVENTS_NAME, isProduction } from '../config';

const storageMiddleware = (store) => (next) => (action) => {
  const { type } = action;
  if (
    type === 'user/logoutUser' ||
    type === 'user/setUser' ||
    type === 'user/removeEvent' ||
    type === 'user/addEvent'
  ) {
    localStorage.setItem(LS_ACTION_NAME, JSON.stringify(action));
  }
  const result = next(action);
  if (type === 'user/removeEvent' || type === 'user/addEvent') {
    const events = store.getState().user.events;
    localStorage.setItem(LS_EVENTS_NAME, JSON.stringify(events));
  }
  return result;
};

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }).concat(storageMiddleware),
});

if (!isProduction && module.hot) {
  module.hot.accept('./rootReducer', () => store.replaceReducer(rootReducer));
}

export default store;
