import { nanoid } from 'nanoid';

const isProduction = process.env.NODE_ENV === 'production';

const LS_ACTION_NAME = 'myAppAction';
const LS_EVENTS_NAME = 'myEvents';
const USER_ID = nanoid(11);
const mainChatName = 'Main';
const drawingName = '[drawing]';
const credentials = isProduction ? 'same-origin' : 'include';
const apiUrl = '/graphql';
const wsUrl = isProduction ? process.env.WS_URL : 'ws://localhost:8000/graphql';

export { isProduction, LS_ACTION_NAME, LS_EVENTS_NAME, USER_ID, mainChatName, drawingName, apiUrl, credentials, wsUrl };
