import { gql } from '@apollo/client';

const ME_QUERY = gql`
  query me {
    me {
      username
      isLogged
      rooms {
        id
        room
        participant
        unreadCount
      }
    }
  }
`;

const GET_MESSAGES = gql`
  query chatQuery($room: String, $cursor: ID, $limit: Int) {
    chatQuery(room: $room, cursor: $cursor, limit: $limit) {
      messages {
        username
        message
        date
        id
      }
      cursor
    }
  }
`;

const GET_CHAT_USERS = gql`
  query chatUsers($room: String, $participant: String) {
    chatUsers(room: $room, participant: $participant) {
      username
      typing
    }
  }
`;

const GET_EVENTS = gql`
  query eventsQuery {
    events {
      name
      date
      id
    }
  }
`;

const FIND_ROOM_QUERY = gql`
  query findRoom($room: String!, $isPrivate: Boolean) {
    findRoom(room: $room, isPrivate: $isPrivate)
  }
`;

const LOGIN_MUTATION = gql`
  mutation logIn($username: String!, $password: String!) {
    logIn(username: $username, password: $password) {
      username
      rooms {
        id
        room
        participant
        unreadCount
      }
      events {
        name
        date
        id
      }
    }
  }
`;

const SIGNUP_MUTATION = gql`
  mutation signUp($username: String!, $password: String!) {
    signUp(username: $username, password: $password)
  }
`;

const LOGOUT_MUTATION = gql`
  mutation logOut {
    logOut {
      username
      isLogged
      rooms {
        id
        room
      }
    }
  }
`;

const ADD_EVENT_MUTATION = gql`
  mutation addEvent($events: [EventInput!]) {
    addEvent(events: $events)
  }
`;

const REMOVE_EVENT_MUTATION = gql`
  mutation removeEvent($id: ID!) {
    removeEvent(id: $id)
  }
`;

const ADD_MESSAGE_MUTATION = gql`
  mutation addMessage($room: String!, $message: String!, $date: Date!, $id: ID!, $clientId: ID!, $participant: String) {
    addMessage(room: $room, message: $message, date: $date, id: $id, clientId: $clientId, participant: $participant) {
      username
      message
      date
      room
      id
    }
  }
`;

const USER_TYPING_MUTATION = gql`
  mutation userTyping($room: String!, $clientId: ID!, $typing: Boolean!) {
    userTyping(room: $room, clientId: $clientId, typing: $typing)
  }
`;

const JOIN_ROOM_MUTATION = gql`
  mutation joinRoom($room: String!, $leave: Boolean!, $clientId: ID!, $byRoomName: Boolean) {
    joinRoom(room: $room, leave: $leave, clientId: $clientId, byRoomName: $byRoomName) {
      id
      unreadCount
    }
  }
`;

const CREATE_ROOM_MUTATION = gql`
  mutation createRoom($clientId: ID!, $room: String, $participant: String) {
    createRoom(clientId: $clientId, room: $room, participant: $participant) {
      id
      room
      participant
    }
  }
`;

const UNREAD_CHAT_MUTATION = gql`
  mutation setUnreadChat($clientId: ID!, $room: String, $participant: String) {
    setUnreadChat(clientId: $clientId, room: $room, participant: $participant)
  }
`;

const CHATS_SUBSCRIPTION = gql`
  subscription chatAdded($rooms: [String]!, $clientId: ID!, $username: String!) {
    chatAdded(rooms: $rooms, clientId: $clientId, username: $username) {
      username
      message
      date
      id
      room
    }
  }
`;

const CHAT_USERS_SUBSCRIPTION = gql`
  subscription userConnected($username: String!, $room: String, $participant: String) {
    userConnected(username: $username, room: $room, participant: $participant) {
      username
      prevUsername
      connected
    }
  }
`;

const USER_TYPING_SUBSCRIPTION = gql`
  subscription userTypingSubscription($username: String!, $room: String) {
    userTypingSubscription(username: $username, room: $room) {
      username
      prevUsername
      typing
    }
  }
`;

const CHANGE_CHAT_SUBSCRIPTION = gql`
  subscription changeChat($username: String!, $clientId: ID!) {
    changeChat(username: $username, clientId: $clientId) {
      room
      participant
      prevRoom
    }
  }
`;

export {
  ME_QUERY,
  GET_MESSAGES,
  GET_CHAT_USERS,
  GET_EVENTS,
  FIND_ROOM_QUERY,
  LOGIN_MUTATION,
  SIGNUP_MUTATION,
  LOGOUT_MUTATION,
  ADD_EVENT_MUTATION,
  REMOVE_EVENT_MUTATION,
  ADD_MESSAGE_MUTATION,
  USER_TYPING_MUTATION,
  JOIN_ROOM_MUTATION,
  CREATE_ROOM_MUTATION,
  UNREAD_CHAT_MUTATION,
  USER_TYPING_SUBSCRIPTION,
  CHATS_SUBSCRIPTION,
  CHAT_USERS_SUBSCRIPTION,
  CHANGE_CHAT_SUBSCRIPTION,
};
