import { ApolloClient, HttpLink, InMemoryCache, split, concat } from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import { getMainDefinition } from '@apollo/client/utilities';
import { BatchHttpLink } from '@apollo/client/link/batch-http';
import { ApolloLink, Observable } from '@apollo/client/core';
import { createClient } from 'graphql-ws';
import { print } from 'graphql';
import { USER_ID, apiUrl, wsUrl, credentials } from '../config';
import store from '../redux/store';

const httpLink = new HttpLink({
  uri: apiUrl,
  credentials,
});

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) =>
      console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`),
    );
  }
  if (networkError) console.log(`[Network error]: ${networkError}`);
});

const batchLink = new BatchHttpLink({
  uri: apiUrl,
  credentials,
});

class WebSocketLink extends ApolloLink {
  constructor(options) {
    super();
    this.client = createClient(options);
  }

  request(operation) {
    return new Observable((sink) => {
      return this.client.subscribe(
        { ...operation, query: print(operation.query) },
        {
          next: sink.next.bind(sink),
          complete: sink.complete.bind(sink),
          error: (err) => {
            if (err instanceof Error) {
              return sink.error(err);
            }

            if (err instanceof CloseEvent) {
              return sink.error(new Error(`Socket closed with event ${err.code} ${err.reason || ''}`));
            }

            return sink.error(new Error(err.map(({ message }) => message).join(', ')));
          },
        },
      );
    });
  }
}

const wsLink = new WebSocketLink({
  url: wsUrl,
  connectionParams: () => ({
    id: USER_ID,
    rooms: store
      .getState()
      .user.rooms.map((el) => el.id)
      .filter(Boolean),
  }),
});

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  concat(errorLink, batchLink, httpLink),
);

const { client: subscriptionClient } = wsLink;

const cache = new InMemoryCache({
  typePolicies: {
    ChatResponse: {
      fields: {
        messages: {
          merge(prev = [], next, { variables }) {
            if (Array.isArray(next)) {
              if (variables.cursor === null && prev.length > 0) {
                const prevNames = prev.map((el) => el.__ref);
                let insertIndex;
                const items = next.filter((el, i) => {
                  if (prevNames.indexOf(el.__ref) === -1) {
                    insertIndex = i;
                    return true;
                  }
                  return false;
                });

                if (items.length === 0) {
                  return next.length > prev.length ? [...next] : prev;
                }

                insertIndex -= items.length;
                const res = [...prev];
                res.splice(prevNames.indexOf(next[insertIndex]?.__ref) + 1, 0, ...items);
                return res;
              }

              return [...next, ...(Array.isArray(prev) ? prev : [prev])];
            }

            return [...prev, next];
          },
        },
        cursor: {
          merge(prev = null, next) {
            return next === '' ? prev : next;
          },
        },
      },
    },
    Query: {
      fields: {
        chatQuery: {
          keyArgs: ['room'],
          merge: true,
        },
        chatUsers: {
          merge: false,
        },
      },
    },
  },
});

const client = new ApolloClient({ link, cache, defaultHttpLink: false });

export { subscriptionClient };

export default client;
