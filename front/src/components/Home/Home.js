import { useSelector, useDispatch } from 'react-redux';
import { toggleModal } from '../../redux/modalReducer';
import Modal from '../Modal/Modal';
import Login from '../Modal/Login';

function Home() {
  const username = useSelector((state) => state.user.username);
  const logged = useSelector((state) => state.user.logged);
  const dispatch = useDispatch();

  function handleClick(val) {
    dispatch(toggleModal({ visibility: true, signUp: val, canvas: false }));
  }

  return (
    <>
      <Modal>
        <Login />
      </Modal>
      <div className="home">
        <h1 className="home-title">Very exciting homepage</h1>
        {logged ? (
          <p>
            Welcome <span className="home-user">{username}</span>
          </p>
        ) : (
          <p>
            You can{' '}
            <button className="home-link" onClick={() => handleClick(false)}>
              Log In
            </button>{' '}
            or{' '}
            <button className="home-link" onClick={() => handleClick(true)}>
              Register
            </button>{' '}
            for better user experience
          </p>
        )}
      </div>
    </>
  );
}

export default Home;
