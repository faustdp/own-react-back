import loadable from '@loadable/component';

const LoadableHome = loadable(() => import(/* webpackChunkName: 'home' */ './Home'), {
  fallback: null,
});

export default LoadableHome;
