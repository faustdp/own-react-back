import { useCallback, useState, useRef, useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import nanomemoize from 'nano-memoize';

function Days({ days, firstDay, date, currentDate, selectedDate, updates, direction, startDay }) {
  const [view, setView] = useState(updates);
  const rerender = useRef(true);
  const daysArr = useRef([]);
  const cont = useRef();
  const dates = useRef();

  const handleTransitionEnd = useCallback((e) => {
    if (e.target !== cont.current) return;
    rerender.current = false;
    setView((prevState) => !prevState);
  }, []);

  const getDates = useCallback(
    () => ({
      currYear: currentDate.getFullYear(),
      currMonth: currentDate.getMonth(),
      currDate: currentDate.getDate(),
      otherYear: date.getFullYear(),
      otherMonth: date.getMonth(),
    }),
    [currentDate, date],
  );

  const getSelectedDates = useCallback(() => {
    if (selectedDate) {
      dates.current.selectedYear = selectedDate.getFullYear();
      dates.current.selectedMonth = selectedDate.getMonth();
      dates.current.selectedDate = selectedDate.getDate();
    }
  }, [selectedDate]);

  const getCurrentMonthDays = useCallback(() => {
    let currentDay, selectedDay;
    if (!updates) {
      if (dates.current.otherYear === dates.current.currYear && dates.current.otherMonth === dates.current.currMonth) {
        currentDay = dates.current.currDate;
      }
      if (
        selectedDate &&
        dates.current.otherYear === dates.current.selectedYear &&
        dates.current.otherMonth === dates.current.selectedMonth
      ) {
        selectedDay = dates.current.selectedDate;
      }
    }
    for (let i = 1; i <= days; i++) {
      daysArr.current.push({
        month: 'current',
        html: i,
        currentDay: currentDay && currentDay === i,
        selectedDay: selectedDay && selectedDay === i,
      });
    }
  }, [updates, days, selectedDate]);

  const getPrevMonthDays = useCallback(() => {
    if (firstDay !== startDay) {
      let currentDay, prevMonth, prevDays, selectedDay;
      if (!updates) {
        if (
          (dates.current.otherYear === dates.current.currYear &&
            dates.current.otherMonth === dates.current.currMonth + 1) ||
          (dates.current.otherYear === dates.current.currYear + 1 &&
            dates.current.otherMonth === 0 &&
            dates.current.currMonth === 11)
        ) {
          currentDay = dates.current.currDate;
        }
        if (
          selectedDate &&
          ((dates.current.otherYear === dates.current.selectedYear &&
            dates.current.otherMonth === dates.current.selectedMonth + 1) ||
            (dates.current.otherYear === dates.current.selectedYear + 1 &&
              dates.current.otherMonth === 0 &&
              dates.current.selectedMonth === 11))
        ) {
          selectedDay = dates.current.selectedDate;
        }
      }
      if (dates.current.otherMonth !== 0) {
        prevMonth = new Date(dates.current.otherYear, dates.current.otherMonth, 0);
      } else {
        prevMonth = new Date(dates.current.otherYear - 1, 11, 31);
      }
      prevDays = prevMonth.getDate();
      const days = startDay > firstDay ? 6 : firstDay - startDay;
      for (let i = 0; i < days; i++) {
        daysArr.current.unshift({
          month: 'prev',
          html: prevDays,
          currentDay: currentDay && currentDay === prevDays,
          selectedDay: selectedDay && selectedDay === prevDays,
        });
        prevDays--;
      }
    }
  }, [firstDay, updates, selectedDate, startDay]);

  const getNextMonthDays = useCallback(() => {
    let calendarEnd = daysArr.current.length % 7;
    if (calendarEnd !== 0) {
      let currentDay, selectedDay;
      if (!updates) {
        if (
          (dates.current.otherYear === dates.current.currYear &&
            dates.current.otherMonth === dates.current.currMonth - 1) ||
          (dates.current.otherYear === dates.current.currYear - 1 &&
            dates.current.otherMonth === 11 &&
            dates.current.currMonth === 0)
        ) {
          currentDay = dates.current.currDate;
        }
        if (
          selectedDate &&
          ((dates.current.otherYear === dates.current.selectedYear &&
            dates.current.otherMonth === dates.current.selectedMonth - 1) ||
            (dates.current.otherYear === dates.current.selectedYear - 1 &&
              dates.current.otherMonth === 11 &&
              dates.current.selectedMonth === 0))
        ) {
          selectedDay = dates.current.selectedDate;
        }
      }
      for (let i = 1, diff = 7 - calendarEnd; i <= diff; i++) {
        daysArr.current.push({
          month: 'next',
          html: i,
          currentDay: currentDay && currentDay === i,
          selectedDay: selectedDay && selectedDay === i,
        });
      }
    }
  }, [daysArr, updates, selectedDate]);

  const renderDays = nanomemoize((days) =>
    days.map((day) => (
      <button
        className={classNames(day.month, {
          'current--day': day.currentDay,
          'is-selected': day.selectedDay,
        })}
        key={`${day.html + day.month}`}
      >
        {day.html}
      </button>
    )),
  );

  useEffect(() => {
    cont.current.inert = view;
  }, [view]);

  useEffect(() => {
    if (!rerender.current) rerender.current = true;
  });

  if (!updates && rerender.current) {
    daysArr.current = [];
    dates.current = getDates();
    getSelectedDates();
    getCurrentMonthDays();
    getPrevMonthDays();
    getNextMonthDays();
  }

  const RenderedDays = renderDays(daysArr.current);

  return (
    <div
      className={classNames('days', view ? 'is-hidden' : 'is-visible', rerender.current ? direction : '')}
      ref={cont}
      onTransitionEnd={handleTransitionEnd}
    >
      {RenderedDays}
    </div>
  );
}

Days.propTypes = {
  days: PropTypes.number,
  firstDay: PropTypes.number,
  date: PropTypes.object,
  currentDate: PropTypes.object,
  selectedDate: PropTypes.object,
  updates: PropTypes.bool,
  direction: PropTypes.string,
  startDay: PropTypes.number,
};

export default memo(Days);
