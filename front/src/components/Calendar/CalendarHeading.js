import { Fragment } from 'react';
import PropTypes from 'prop-types';

function CalendarHeading({ title, info, titleRef, subTitleRef }) {
  return (
    <Fragment>
      <h1 className="calendar__title" ref={titleRef}>
        {title}
      </h1>
      <p className="calendar__title" ref={subTitleRef}>
        {info}
      </p>
    </Fragment>
  );
}

CalendarHeading.propTypes = {
  title: PropTypes.string,
  info: PropTypes.string,
  titleRef: PropTypes.object.isRequired,
  subTitleRef: PropTypes.object.isRequired,
};

export default CalendarHeading;
