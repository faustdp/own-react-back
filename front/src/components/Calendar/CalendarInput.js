import { Fragment, useCallback } from 'react';
import PropTypes from 'prop-types';

function CalendarInput({ newEvent, error, inputRef }) {
  const handleChange = useCallback(
    (e) => {
      newEvent.current = e.target.value;
    },
    [newEvent],
  );

  const errorMsg =
    error === 'inputLength'
      ? 'Event length must be atleast 3 characters long'
      : error === 'eventExists'
      ? 'Such event already exist'
      : error === 'tooEarly'
      ? 'Forget about past. Future is coming'
      : false;

  return (
    <Fragment>
      <input
        type="text"
        placeholder="type something"
        className="calendar__input"
        id="calendar__input"
        onChange={handleChange}
        ref={inputRef}
      />
      {errorMsg && (
        <label className="calendar__input--error" htmlFor="calendar__input">
          {errorMsg}
        </label>
      )}
    </Fragment>
  );
}

CalendarInput.propTypes = {
  newEvent: PropTypes.object.isRequired,
  error: PropTypes.string,
  inputRef: PropTypes.object.isRequired,
};

export default CalendarInput;
