import { memo } from 'react';
import PropTypes from 'prop-types';

const Weeks = memo(function Weeks({ lang, startDay }) {
  const baseDate = new Date();
  const baseDay = baseDate.getDay();
  const week = [];
  baseDate.setDate(baseDate.getDate() - ((7 + baseDay - startDay) % 7));
  for (let i = 0; i < 7; i++) {
    week.push(baseDate.toLocaleString(lang, { weekday: 'short' }));
    baseDate.setDate(baseDate.getDate() + 1);
  }
  const weekList = week.map((day) => <span key={day}>{day}</span>);
  return <div className="weeks">{weekList}</div>;
});

Weeks.propTypes = {
  lang: PropTypes.string.isRequired,
  startDay: PropTypes.number.isRequired,
};

export default Weeks;
