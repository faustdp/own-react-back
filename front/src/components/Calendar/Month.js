import PropTypes from 'prop-types';

function Month({ monthName, year, onClick }) {
  function handlePrevArrow() {
    onClick('back');
  }

  return (
    <div className="month">
      <button className="month__arrow month__arrow--prev" onClick={handlePrevArrow}>
        ◄
      </button>
      <span className="month__title">
        {monthName}, {year}
      </span>
      <button className="month__arrow month__arrow--next" onClick={onClick}>
        ►
      </button>
    </div>
  );
}

Month.propTypes = {
  monthName: PropTypes.string.isRequired,
  year: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Month;
