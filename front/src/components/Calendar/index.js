import loadable from '@loadable/component';

const LoadableCalendar = loadable(() => import(/* webpackChunkName: 'calendar' */ './Calendar'), {
  fallback: null,
});

export default LoadableCalendar;
