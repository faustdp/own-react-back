import PropTypes from 'prop-types';

function EventSubmit({ onClick, text, btnRef }) {
  return (
    <button className="btn" onClick={onClick} ref={btnRef}>
      {text}
    </button>
  );
}

EventSubmit.propTypes = {
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string,
  btnRef: PropTypes.object.isRequired,
};

export default EventSubmit;
