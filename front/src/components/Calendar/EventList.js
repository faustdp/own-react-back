import { memo, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { REMOVE_EVENT_MUTATION } from '../../api/queries';
import { useMutation } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import { removeEvent } from '../../redux/userReducer';

const dayInMs = 1000 * 60 * 60 * 24;

function EventList({ lang, events, eventRef }) {
  const [removeEventMutation] = useMutation(REMOVE_EVENT_MUTATION, { ignoreResults: true });
  const logged = useSelector((state) => state.user.logged);
  const dispatch = useDispatch();

  const handleEventRemove = useCallback(
    async (id) => {
      if (logged) {
        try {
          await removeEventMutation({ variables: { id } });
        } catch (err) {
          console.log(err);
        }
      }
      dispatch(removeEvent(id));
    },
    [dispatch, removeEventMutation, logged],
  );

  useEffect(() => {
    const today = Date.now();
    events.forEach((evt) => {
      if (today - new Date(evt.date).getTime() > dayInMs * 3) {
        handleEventRemove(evt.id);
      }
    });
  }, [events, handleEventRemove]);

  const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    weekday: 'long',
  };

  const sortedEvents = [...events].sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime());

  return (
    <div className="events" ref={eventRef}>
      {sortedEvents.map((evt) => (
        <div className="event" key={evt.id}>
          <p className="event-name">{evt.name}</p>
          <p className="event-date">{new Date(evt.date).toLocaleString(lang, options)}</p>
          <span className="event-remove" onClick={handleEventRemove.bind(this, evt.id)}>
            x
          </span>
        </div>
      ))}
    </div>
  );
}

EventList.propTypes = {
  lang: PropTypes.string.isRequired,
  events: PropTypes.array.isRequired,
  eventRef: PropTypes.object.isRequired,
};

export default memo(EventList);
