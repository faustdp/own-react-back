import { useState, useEffect, useRef } from 'react';
import throttle from '../../helpers/throttle';
import { usePrevious } from '../../helpers/hooks';
import CalendarHeading from './CalendarHeading';
import CalendarInput from './CalendarInput';
import Month from './Month';
import Weeks from './Weeks';
import Days from './Days';
import EventList from './EventList';
import EventSubmit from './EventSubmit';
import { nanoid } from 'nanoid';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { changeDate, selectDate, setCurrentDate, updateView } from '../../redux/calendarReducer';
import { addEvent } from '../../redux/userReducer';
import PropTypes from 'prop-types';
import withAnimation from '../withAnimation';
import { gsap } from 'gsap';
import { GET_EVENTS, ADD_EVENT_MUTATION } from '../../api/queries';
import { useQuery, useMutation } from '@apollo/client';
import { Helmet } from 'react-helmet-async';

const animDelay = 0.35;
const animDuration = 0.3;
const animStagger = 0.1;

const KEYS = {
  LEFT: 37,
  UP: 38,
  RIGHT: 39,
  DOWN: 40,
  SPACE: 32,
  ENTER: 13,
};

const language = navigator.language;
const SUNDAY_COUNTRIES = `AG AS AU BD BR BS BT BW BZ CA CN CO DM DO EN ET GT GU HK HN ID IL IN JM JP KE KH LA MH MM MO
 MT MX MZ NI NP PA PE PH PK PR PT PY SA SG SV TH TT TW UM US VE VI WS YE ZA ZW`;
const country = language.includes('-') ? language.split('-')[1].toUpperCase() : language.toUpperCase();
const startDay = SUNDAY_COUNTRIES.includes(country) ? 0 : 1;

function Calendar({ onTransitionEnd, path, ready, delay, setDelay }) {
  const [error, errorSet] = useState('');
  const newEvent = useRef('');
  const view = useRef('');
  const anim = useRef(false);
  const changeTO = useRef(null);
  const errorTO = useRef(null);
  const title = useRef(null);
  const subTitle = useRef(null);
  const input = useRef(null);
  const datepicker = useRef(null);
  const btn = useRef(null);
  const eventList = useRef(null);
  const daysRef = useRef(null);
  const tween = useRef();
  const { date, currentDate, selectedDate, updates, events, logged } = useSelector(
    ({ calendar: { date, currentDate, selectedDate, updates }, user: { events, logged } }) => ({
      date,
      currentDate,
      selectedDate,
      updates,
      events,
      logged,
    }),
    shallowEqual,
  );
  const dispatch = useDispatch();
  const prevReady = usePrevious(ready);
  const [addEventMutation] = useMutation(ADD_EVENT_MUTATION, { ignoreResults: true });

  useQuery(GET_EVENTS, {
    skip: !logged,
    onCompleted: (data) => {
      if (data?.events.length > 0) {
        const indexes = events.map((el) => el.id);
        const filtered = data.events.filter((el) => !indexes.includes(el.id));
        dispatch(addEvent(filtered));
      }
    },
  });

  const handleMonthChange = (direction, changeFocus = false) => {
    if (anim.current) return;
    const prev = direction === 'back';
    const change = prev ? -1 : 1;
    view.current = prev ? 'to-right' : 'to-left';
    anim.current = true;
    dispatch(updateView(!updates));
    dispatch(changeDate(new Date(date.getFullYear(), date.getMonth() + change)));
    clearTimeout(errorTO.current);
    changeTO.current = setTimeout(() => {
      if (changeFocus) {
        const days = daysRef.current.querySelector('.is-visible').children;
        days[view.current === 'to-right' ? days.length - 1 : 0].focus();
      }
      anim.current = false;
      view.current = '';
    }, 400);
  };

  const handleError = (err) => {
    clearTimeout(errorTO.current);
    errorSet(err);
    errorTO.current = setTimeout(() => {
      errorSet('');
    }, 3000);
  };

  const handleClick = (e) => {
    const target = e.target;
    if (target.tagName.toLowerCase() !== 'button') return;
    if (target.classList.contains('is-selected')) {
      dispatch(selectDate(null));
      if (e.type === 'click') {
        target.blur();
      }
      return;
    }
    const month = target.className;
    const change = month.includes('current') ? 0 : month.includes('prev') ? -1 : 1;
    const selectedDay = new Date(date.getFullYear(), date.getMonth() + change, target.innerHTML);
    dispatch(selectDate(selectedDay));
  };

  const changeFocus = (key, target) => {
    if (target.tagName.toLowerCase() !== 'button') return;
    const days = [...target.parentNode.children];
    const len = days.length - 1;
    const index = days.indexOf(target);
    target.blur();
    switch (key) {
      case KEYS.RIGHT:
        if (index + 1 > len) {
          handleMonthChange('forward', true);
        } else {
          days[index + 1].focus();
        }
        break;
      case KEYS.LEFT:
        if (index - 1 < 0) {
          handleMonthChange('back', true);
        } else {
          days[index - 1].focus();
        }
        break;
      case KEYS.DOWN:
        days[index + 7 > len ? index % 7 : index + 7].focus();
        break;
      case KEYS.UP:
        days[index - 7 < 0 ? len - (6 - (index % 7)) : index - 7].focus();
        break;
    }
  };

  const handleKeyDown = (e) => {
    const key = e.which;
    if (key === KEYS.ENTER || key === KEYS.SPACE) {
      handleClick(e);
    } else if (key === KEYS.UP || key === KEYS.DOWN || key === KEYS.LEFT || key === KEYS.RIGHT) {
      changeFocus(key, e.target);
    }
  };

  const handleSubmit = () => {
    const len = events.length;
    const newEventTrim = newEvent.current.trim();
    const today = new Date(currentDate).setHours(0, 0, 0, 0);
    if (newEventTrim.length < 3) {
      return handleError('inputLength');
    }
    if (!selectedDate) {
      return handleError('selectDate');
    }
    if (len) {
      for (let i = 0; i < events.length; i++) {
        if (events[i].event === newEventTrim && +events[i].date === +selectedDate) {
          return handleError('eventExists');
        }
      }
    }
    if (today > +selectedDate) {
      return handleError('tooEarly');
    }
    input.current.value = newEvent.current = '';
    input.current.focus();
    const id = nanoid(11);
    const event = {
      name: newEventTrim,
      date: selectedDate,
      id,
    };
    dispatch(addEvent([event]));
    if (logged) {
      addEventMutation({
        variables: {
          events: [event],
        },
      }).catch((err) => console.log(err));
    }
  };

  const getMonthName = () => date.toLocaleString(language, { month: 'long' });

  const getLeapYear = () => {
    const year = date.getFullYear();
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
  };

  const getDaysInMonth = (month) => {
    const daysInMonth = [31, getLeapYear() ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    return daysInMonth[month];
  };

  useEffect(() => {
    let tomorrowTO = null;

    function changeTomorrowDate() {
      const tomorrow = new Date();
      tomorrow.setDate(currentDate.getDate() + 1);
      tomorrowTO = setTimeout(() => {
        dispatch(setCurrentDate(new Date()));
        changeTomorrowDate();
      }, tomorrow.setHours(0, 0, 0, 0) - Date.now());
    }

    changeTomorrowDate();

    return () => {
      clearTimeout(tomorrowTO);
    };
  }, [currentDate, dispatch]);

  useEffect(() => {
    tween.current = gsap.timeline({
      paused: true,
      onReverseComplete: onTransitionEnd,
      onUpdate: throttle(() => {
        setDelay(path, tween.current.progress() * +tween.current.isActive());
      }, 20),
    });

    const items = [title.current, subTitle.current, input.current, datepicker.current, btn.current, eventList.current];
    items.forEach((el) =>
      tween.current.set(el, {
        z: 30,
        rotationY: items.indexOf(el) % 2 ? 60 : -60,
        transformOrigin: '50% 50% -250px',
      }),
    );
    tween.current.to(items, {
      duration: animDuration,
      opacity: 1,
      z: 0,
      rotationY: 0,
      ease: 'power1',
      stagger: animStagger,
    });

    return () => {
      tween.current.kill();
      clearTimeout(changeTO.current);
      clearTimeout(errorTO.current);
    };
  }, [onTransitionEnd, path, setDelay]);

  useEffect(() => {
    if (!ready && prevReady) {
      if (tween.current.progress() < 0.03) {
        setDelay(path, 0);
        onTransitionEnd();
      } else {
        tween.current.reverse();
      }
    } else if (ready && !prevReady) {
      const tweenDelay = delay * animDelay;
      setDelay(path, tweenDelay);
      gsap.delayedCall(tweenDelay, () => tween.current.play());
    }
  }, [ready, setDelay, onTransitionEnd, delay, prevReady, path]);

  const month = date.getMonth();
  const year = date.getFullYear();
  const firstDay = new Date(year, month, 1).getDay();
  const daysInMonth = getDaysInMonth(month);

  return (
    <div className="calendar">
      <Helmet>
        <title>Calendar</title>
        <meta name="description" content="Simple Calendar Made Not Simple" />
      </Helmet>
      <CalendarHeading title="Calendar" info="Create new Event" titleRef={title} subTitleRef={subTitle} />
      <CalendarInput newEvent={newEvent} error={error} inputRef={input} />
      <div className={`datepicker ${error === 'selectDate' ? 'is-error' : ''}`} ref={datepicker}>
        <Month monthName={getMonthName()} year={year} onClick={handleMonthChange} />
        <Weeks lang={language} startDay={startDay} />
        <div className="days-cont" onClick={handleClick} onKeyDown={handleKeyDown} ref={daysRef}>
          <Days
            days={daysInMonth}
            firstDay={firstDay}
            date={date}
            currentDate={currentDate}
            updates={!updates}
            direction={view.current}
            selectedDate={selectedDate}
            startDay={startDay}
          />
          <Days
            days={daysInMonth}
            firstDay={firstDay}
            date={date}
            currentDate={currentDate}
            updates={updates}
            direction={view.current}
            selectedDate={selectedDate}
            startDay={startDay}
          />
        </div>
      </div>
      <EventSubmit btnRef={btn} onClick={handleSubmit} text="Add New Event" />
      <EventList events={events} lang={language} eventRef={eventList} />
    </div>
  );
}

Calendar.propTypes = {
  ready: PropTypes.bool.isRequired,
  onTransitionEnd: PropTypes.func.isRequired,
  setDelay: PropTypes.func.isRequired,
  delay: PropTypes.number.isRequired,
  path: PropTypes.string.isRequired,
};

export default withAnimation(Calendar);
