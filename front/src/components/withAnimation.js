import { useContext, useCallback, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { matchPath, useLocation } from 'react-router-dom';
import { SwitchContext } from './SwitchContext';
import { useIsMounted } from '../helpers/hooks';

function withAnimation(WrappedComponent) {
  function WithAnimation(props) {
    const [state, setState] = useState({ ready: false, delay: 0 });
    const { delay, prevPath } = useContext(SwitchContext);
    const isMounted = useIsMounted();
    const { pathname } = useLocation();

    const { removeChild, path, strict, exact, sensitive, ...rest } = props;

    const checkMatch = useCallback(
      (pathname) => matchPath(pathname, { path, strict, exact, sensitive }),
      [path, strict, exact, sensitive],
    );

    const handleTransitionEnd = useCallback(() => {
      removeChild(path);
    }, [removeChild, path]);

    useEffect(() => {
      const match = checkMatch(pathname);

      if (isMounted()) {
        if (match) {
          setState({
            ready: true,
            delay: delay[prevPath] || 0,
          });
        } else if (!match && pathname !== prevPath) {
          setState({
            ready: false,
            delay: 0,
          });
        }
      }

      if (!isMounted()) {
        if (match) {
          if (document.readyState === 'complete') {
            setState({
              ready: true,
              delay: prevPath !== '/' && delay[prevPath] ? delay[prevPath] : 0,
            });
          } else {
            window.addEventListener('load', () => {
              setState({
                ready: true,
                delay: 0,
              });
            });
          }
        } else {
          handleTransitionEnd();
        }
      }
    }, [pathname, checkMatch, prevPath, delay, handleTransitionEnd, isMounted]);

    return (
      <WrappedComponent
        ready={state.ready}
        delay={state.delay}
        onTransitionEnd={handleTransitionEnd}
        path={path}
        {...rest}
      />
    );
  }

  WithAnimation.propTypes = {
    path: PropTypes.string.isRequired,
    exact: PropTypes.bool,
    strict: PropTypes.bool,
    sensitive: PropTypes.bool,
    removeChild: PropTypes.func.isRequired,
    setDelay: PropTypes.func.isRequired,
  };

  const wrappedComponentName = WrappedComponent.displayName || WrappedComponent.name || 'Component';
  WithAnimation.displayName = `withAnimation(${wrappedComponentName})`;
  return WithAnimation;
}

export default withAnimation;
