import { useEffect, useCallback, useRef } from 'react';
import { Redirect, useHistory } from 'react-router-dom';
import { Helmet, HelmetProvider } from 'react-helmet-async';
import { useDispatch } from 'react-redux';
import { useQuery } from '@apollo/client';
import { ME_QUERY } from '../api/queries';
import { subscriptionClient } from '../api/client';
import { setUser } from '../redux/userReducer';
import Navigation from './Navigation';
import OwnSwitch from './OwnSwitch';
import LoadableChat from './Chat';
import LoadableCalendar from './Calendar';
import LoadableHome from './Home';
import { LS_ACTION_NAME } from '../config';
import nanomemoize from 'nano-memoize';
import deepEqual from '../helpers/deepEqual';

function App() {
  const dispatch = useDispatch();
  const { listen } = useHistory();

  const getRooms = useRef(
    nanomemoize(
      (rooms) => {
        const filteredRooms = rooms.map(({ id, room, participant }) => ({
          id,
          ...(room ? { room } : { participant }),
        }));
        const unreadRooms = rooms.reduce((acc, curr) => {
          if (curr.unreadCount != null) {
            acc[curr.id] = curr.unreadCount;
          }
          return acc;
        }, {});
        return [filteredRooms, unreadRooms];
      },
      { equals: deepEqual },
    ),
  );

  const saveUser = useCallback(
    ({ username, isLogged, rooms }) => {
      const [filteredRooms, unreadRooms] = getRooms.current(rooms);

      dispatch(
        setUser({
          username,
          logged: isLogged,
          rooms: filteredRooms,
          unreadRooms,
        }),
      );
    },
    [dispatch],
  );

  const { refetch } = useQuery(ME_QUERY, {
    fetchPolicy: 'no-cache',
    onCompleted(data) {
      if (data?.me) saveUser(data.me);
    },
  });

  const handleRefetch = useCallback(async () => {
    const res = await refetch();
    if (res?.data?.me) saveUser(res.data.me);
  }, [refetch, saveUser]);

  useEffect(() => {
    function handleStorageChange(e) {
      if (e.key === LS_ACTION_NAME) {
        const action = JSON.parse(e.newValue);
        const { type } = action;
        if (type === 'user/removeEvent' || type === 'user/addEvent') {
          dispatch(action);
        } else if (type === 'user/setUser') {
          handleRefetch();
        } else if (type === 'user/logoutUser') {
          subscriptionClient.dispose();
        }
      }
    }

    function locationListener({ pathname }) {
      if (pathname === '/chat') handleRefetch();
    }

    window.addEventListener('storage', handleStorageChange);
    window.addEventListener('online', handleRefetch);
    const unsub = listen(locationListener);

    return () => {
      window.removeEventListener('storage', handleStorageChange);
      window.removeEventListener('online', handleRefetch);
      unsub();
    };
  }, [handleRefetch, dispatch, listen]);

  return (
    <HelmetProvider>
      <Helmet>
        <title>OwnReactApp</title>
        <meta name="description" content="Unique description" />
      </Helmet>
      <Navigation />
      <OwnSwitch>
        <LoadableHome exact path="/" />
        <Redirect strict from="/chat/" to="/chat" />
        <LoadableChat path="/chat" />
        <Redirect strict from="/calendar/" to="/calendar" />
        <LoadableCalendar path="/calendar" />
        <Redirect to="/" />
      </OwnSwitch>
    </HelmetProvider>
  );
}

export default App;
