import { createContext } from 'react';

const SwitchContext = createContext();

export { SwitchContext };
