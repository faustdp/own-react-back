import { useState, useEffect, useRef, useMemo, useCallback, useLayoutEffect } from 'react';
import PropTypes from 'prop-types';
import ChatUsers from './ChatUsers';
import ChatRoom from './ChatRoom';
import throttle from '../../helpers/throttle';
import cloneDeep from '../../helpers/cloneDeep';
import { usePrevious } from '../../helpers/hooks';
import withAnimation from '../withAnimation';
import { gsap } from 'gsap';
import {
  GET_CHAT_USERS,
  CHAT_USERS_SUBSCRIPTION,
  USER_TYPING_SUBSCRIPTION,
  FIND_ROOM_QUERY,
  JOIN_ROOM_MUTATION,
  UNREAD_CHAT_MUTATION,
} from '../../api/queries';
import { useQuery, useSubscription, useLazyQuery, useMutation } from '@apollo/client';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { changeRooms, setUnreadRoom } from '../../redux/userReducer';
import { Helmet } from 'react-helmet-async';
import { USER_ID, mainChatName } from '../../config';

const animDelay = 0.42;
const animDuration = 0.3;
const animStagger = 0.15;

function Chat({ ready, onTransitionEnd, setDelay, delay, path }) {
  const { username, logged, rooms } = useSelector(
    ({ user: { username, logged, rooms } }) => ({ username, logged, rooms }),
    shallowEqual,
  );
  const [activeRoomId, setActiveRoomId] = useState(rooms[0].id);
  const [activeRoom, setActiveRoom] = useState(mainChatName);
  const [users, setUsers] = useState([]);
  const [restoreScroll, setRestoreScroll] = useState(false);
  const [isPrivate, setIsPrivate] = useState(false);
  const dispatch = useDispatch();
  const titleRef = useRef(null);
  const usersRef = useRef(null);
  const roomRef = useRef(null);
  const searchRef = useRef(null);
  const tween = useRef();
  const participant = useRef();
  const socketUsers = useRef(new Map());
  const isFetched = useRef(false);
  const usernameRef = useRef();
  const typingUsers = useRef([]);
  const prevLogged = usePrevious(logged, logged);
  const prevReady = usePrevious(ready);

  const roomIds = useMemo(() => rooms.map((room) => room.id).filter(Boolean), [rooms]);

  const [unreadChatMutation] = useMutation(UNREAD_CHAT_MUTATION, { ignoreResults: true });

  const [joinRoomMutation] = useMutation(JOIN_ROOM_MUTATION, {
    ignoreResults: true,
    onCompleted(data) {
      if (!(data.joinRoom?.unreadCount >= 0)) return;
      if (data.joinRoom.unreadCount === 0) {
        dispatch(setUnreadRoom({ room: data.joinRoom.id, unreadCount: 0 }));
      } else if (data.joinRoom.id === activeRoomId) {
        unreadChatMutation({
          variables: { room: data.joinRoom.id, participant: participant.current, clientId: USER_ID },
        });
      }
    },
  });

  const {
    data: usersData,
    loading,
    refetch,
  } = useQuery(GET_CHAT_USERS, {
    variables: {
      room: activeRoomId,
      participant: isPrivate ? activeRoom : null,
    },
    skip: activeRoomId === null,
    fetchPolicy: 'no-cache',
  });

  const [findRoom, { data: findRoomData }] = useLazyQuery(FIND_ROOM_QUERY, {
    fetchPolicy: 'no-cache',
  });

  const setSocketUsers = useCallback((username, prevUsername, action, value) => {
    const user = socketUsers.current.has(username)
      ? socketUsers.current.get(username)
      : socketUsers.current.set(username, {}).get(username);
    if (prevUsername) {
      if (!socketUsers.current.has(prevUsername)) {
        socketUsers.current.set(prevUsername, { [action]: false });
      } else {
        socketUsers.current.get(prevUsername)[action] = false;
      }
      user[action] = true;
    } else {
      user[action] = value;
    }
  }, []);

  const getTypingUsers = useCallback(
    (usersArr) => {
      const nextTypingUsers = usersArr.reduce(
        (total, curr) => (curr.typing && curr.username !== username && total.push(curr.username), total),
        [],
      );
      if (
        nextTypingUsers.length !== typingUsers.current.length ||
        nextTypingUsers.some((user) => !typingUsers.current.includes(user))
      ) {
        typingUsers.current = nextTypingUsers;
      }
    },
    [username],
  );

  useSubscription(CHAT_USERS_SUBSCRIPTION, {
    variables: { username, room: activeRoomId, participant: isPrivate ? activeRoom : null },
    skip: activeRoomId === null,
    fetchPolicy: 'no-cache',
    onSubscriptionData({ subscriptionData }) {
      const { connected, username, prevUsername } = subscriptionData.data.userConnected;

      if (!isFetched.current) {
        return setSocketUsers(username, prevUsername, 'connected', connected);
      }

      setUsers((prevUsers) => {
        const userExists = prevUsers.some((el) => el.username === username);
        if ((connected && userExists) || (connected === false && !userExists)) {
          return prevUsers;
        }
        const users = cloneDeep(prevUsers);
        const name = prevUsername || username;
        const ind = users.findIndex((el) => el.username === name);
        if (prevUsername && ind !== -1) {
          users[ind].username = username;
        } else if (connected) {
          users.push({
            typing: false,
            username,
          });
        } else if (ind !== -1) {
          users.splice(ind, 1);
        }
        getTypingUsers(users);
        return users;
      });
    },
  });

  useSubscription(USER_TYPING_SUBSCRIPTION, {
    variables: { username, room: activeRoomId },
    skip: !activeRoomId,
    fetchPolicy: 'no-cache',
    onSubscriptionData({ subscriptionData }) {
      const { typing, username, prevUsername } = subscriptionData.data.userTypingSubscription;

      if (!isFetched.current) {
        return setSocketUsers(username, prevUsername, 'typing', typing);
      }

      setUsers((prevUsers) => {
        const name = prevUsername || username;
        const index = prevUsers.findIndex((el) => el.username === name);
        if (index === -1 && !prevUsername) return prevUsers;
        const users = cloneDeep(prevUsers);
        if (prevUsername) {
          users[index].username = username;
        } else {
          users[index].typing = typing;
        }
        getTypingUsers(users);
        return users;
      });
    },
  });

  const handleActiveRoom = useCallback((name, id, isPrivate = false, fetched = false) => {
    isFetched.current = fetched;
    socketUsers.current.clear();
    participant.current = isPrivate ? name : null;
    setActiveRoom(name);
    setActiveRoomId(id);
    setIsPrivate(isPrivate);
  }, []);

  const handleUsersClick = useCallback(
    (user) => {
      if (activeRoom === user) return;
      const roomExists = rooms.find((el) => el.participant === user);
      if (roomExists) {
        handleActiveRoom(roomExists.participant, roomExists.id, true);
      } else {
        dispatch(changeRooms({ room: { id: '', participant: user } }));
        handleActiveRoom(user, '', true);
        findRoom({ variables: { room: user, isPrivate: true } });
      }
    },
    [findRoom, rooms, activeRoom, handleActiveRoom, dispatch],
  );

  useEffect(() => {
    if (findRoomData?.findRoom) {
      dispatch(changeRooms({ room: { id: findRoomData.findRoom[0], participant: participant.current }, change: true }));
      setActiveRoomId(findRoomData.findRoom[0]);
      joinRoomMutation({
        variables: {
          room: findRoomData.findRoom[0],
          leave: false,
          clientId: USER_ID,
        },
      });
    }
  }, [findRoomData, dispatch, joinRoomMutation]);

  useEffect(() => {
    if (usersData?.chatUsers) {
      isFetched.current = true;

      setUsers(() => {
        const users = cloneDeep(usersData.chatUsers);

        if (!users.some((user) => user.username === usernameRef.current)) {
          users.push({
            username: usernameRef.current,
            typing: false,
          });
        }

        socketUsers.current.forEach((val, key) => {
          const userIndex = users.findIndex((el) => el.username === key);
          if (userIndex === -1 && val.connected) {
            users.push({
              username: key,
              typing: !!val.typing,
            });
          }

          if (userIndex !== -1) {
            if (val.connected === false) {
              users.splice(userIndex, 1);
            }
            if ('typing' in val) {
              users[userIndex].typing = val.typing;
            }
          }
        });
        socketUsers.current.clear();
        getTypingUsers(users);
        return users;
      });
    }
  }, [usersData, getTypingUsers]);

  useEffect(() => {
    if (username && usernameRef.current) {
      refetch();
      const name = usernameRef.current;
      setUsers((prevUsers) => {
        if (prevUsers.some((el) => el.username === username)) return prevUsers;
        const users = cloneDeep(prevUsers);
        const user = users.find((el) => el.username === name);
        if (user) {
          user.username = username;
        }
        getTypingUsers(users);
        return users;
      });
    }
    usernameRef.current = username;
  }, [username, refetch, getTypingUsers]);

  useEffect(() => {
    tween.current = gsap.timeline({
      paused: true,
      onReverseComplete: onTransitionEnd,
      onUpdate: throttle(() => {
        setDelay(path, tween.current.progress() * +tween.current.isActive());
      }, 20),
    });

    const items = [titleRef.current, usersRef.current, roomRef.current, searchRef.current];
    tween.current.to(items, {
      duration: animDuration,
      opacity: 1,
      y: 0,
      ease: 'power1',
      stagger: animStagger,
    });

    return () => tween.current.kill();
  }, [onTransitionEnd, path, setDelay]);

  useEffect(() => {
    if (!ready && prevReady) {
      if (tween.current.progress() < 0.03) {
        setDelay(path, 0);
        onTransitionEnd();
      } else {
        tween.current.reverse();
      }
      setRestoreScroll(true);
    } else if (ready && !prevReady) {
      const tweenDelay = delay * animDelay;
      setDelay(path, tweenDelay);
      gsap.delayedCall(tweenDelay, () => tween.current.play());
      if (restoreScroll) setRestoreScroll(false);
    }
  }, [ready, prevReady, setDelay, onTransitionEnd, delay, restoreScroll, path]);

  useEffect(() => {
    if (activeRoomId == null && rooms[0].id) {
      setActiveRoomId(rooms[0].id);
    }
  }, [activeRoomId, rooms]);

  useLayoutEffect(() => {
    if (!logged && prevLogged) {
      handleActiveRoom(mainChatName, rooms[0].id, false, true);
    }
  }, [logged, handleActiveRoom, prevLogged, rooms]);

  return (
    <div className="chat">
      <Helmet>
        <title>Chat</title>
        <meta name="description" content="Telegram v3.0 beta" />
      </Helmet>
      <h1 ref={titleRef} className="chat-title">
        Chat
      </h1>
      <ChatUsers
        users={users}
        animRef={usersRef}
        username={username}
        loading={loading}
        handleUsersClick={handleUsersClick}
      />
      <ChatRoom
        animRef={roomRef}
        searchRef={searchRef}
        typingUsers={typingUsers.current}
        restoreScroll={restoreScroll}
        username={username}
        activeRoom={activeRoom}
        activeRoomId={activeRoomId}
        roomIds={roomIds}
        rooms={rooms}
        handleActiveRoom={handleActiveRoom}
        handleUsersClick={handleUsersClick}
        participant={participant}
      />
    </div>
  );
}

Chat.propTypes = {
  ready: PropTypes.bool.isRequired,
  onTransitionEnd: PropTypes.func.isRequired,
  setDelay: PropTypes.func.isRequired,
  delay: PropTypes.number.isRequired,
  path: PropTypes.string.isRequired,
};

export default withAnimation(Chat);
