import { memo, useCallback, useEffect, useLayoutEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { useMutation, useQuery, useSubscription } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import NimbleEmojiIndex from 'emoji-mart/dist-es/utils/emoji-index/nimble-emoji-index';
import { nanoid } from 'nanoid';
import { changeRooms, setUnreadRoom } from '../../redux/userReducer';
import { subscriptionClient } from '../../api/client';
import {
  ADD_MESSAGE_MUTATION,
  CHATS_SUBSCRIPTION,
  CREATE_ROOM_MUTATION,
  FIND_ROOM_QUERY,
  GET_MESSAGES,
  JOIN_ROOM_MUTATION,
  UNREAD_CHAT_MUTATION,
  CHANGE_CHAT_SUBSCRIPTION,
  USER_TYPING_MUTATION,
} from '../../api/queries';
import { mainChatName, USER_ID } from '../../config';
import animate, { Back, Circular, Sinusoidal } from '../../helpers/animation';
import { useIsMounted } from '../../helpers/hooks';
import throttle from '../../helpers/throttle';
import ChatInput from './ChatInput';
import ChatList from './ChatList';
import ChatTab from './ChatTab';

const arrowAnimationDistance = 200;
const arrowAnimationTime = 350;
const chatsBottomScrollAnimation = 300;
const chatsLoadOnScroll = 10;
const chatsLoadOnQuery = 15;
const loadingHeight = 45;
const msgAddingHeight = 80;
const loadOnScrollOffset = 120;
const smoothScrollOffset = 240;

const fixStringForRegexp = (str) => str.replace(/\s+/g, ' ').replace(/[^\s\w-]/g, '');

const ChatRoom = ({
  animRef,
  searchRef,
  typingUsers,
  restoreScroll,
  username,
  activeRoom,
  activeRoomId,
  roomIds,
  rooms,
  handleActiveRoom,
  handleUsersClick,
  participant,
}) => {
  const unreadRooms = useSelector((state) => state.user.unreadRooms);
  const [arrows, setArrows] = useState('none');
  const [showLoadOnScroll, setShowLoadOnScroll] = useState(false);
  const [createRoomError, createRoomErrorSet] = useState('');
  const [msgAdding, msgAddingSet] = useState(false);
  const [emojiData, emojiDataSet] = useState(null);
  const [roomSearch, setRoomSearch] = useState('');
  const throttledSetRoomSearch = useRef(throttle(setRoomSearch, 170));
  const chatStatus = useRef({
    isTyping: false,
    submiting: false,
    chatsFetched: false,
    loadingOnScroll: false,
    loadedOnScroll: false,
    isAnimating: false,
    joiningRoom: false,
    layoutChanged: false,
    roomIsCreating: '',
    messagesHeight: 0,
    messagesScroll: 0,
    activeRoomIdRef: null,
    errorTimer: null,
    roomsLen: [rooms.length, Object.keys(unreadRooms).length],
    animRef: animate({
      duration: chatsBottomScrollAnimation,
      timing: Sinusoidal.Out,
      cb() {
        chatStatus.current.layoutChanged = false;
      },
      paused: true,
    }),
  });
  const roomsState = useRef(new Map());
  const contRef = useRef(null);
  const tabsContRef = useRef(null);
  const tabsRef = useRef(null);
  const messagesRef = useRef(null);
  const emojiIndex = useRef();
  const isMounted = useIsMounted();
  const dispatch = useDispatch();
  const [addMessageMutation] = useMutation(ADD_MESSAGE_MUTATION, { ignoreResults: true });
  const [userTypingMutation] = useMutation(USER_TYPING_MUTATION, { ignoreResults: true });
  const [joinRoomMutation] = useMutation(JOIN_ROOM_MUTATION, { ignoreResults: true });
  const [createRoomMutation] = useMutation(CREATE_ROOM_MUTATION, { ignoreResults: true });
  const [unreadChatMutation] = useMutation(UNREAD_CHAT_MUTATION, { ignoreResults: true });

  const { data: roomQueryData, loading: roomQueryLoad } = useQuery(FIND_ROOM_QUERY, {
    variables: {
      room: roomSearch,
    },
    skip: roomSearch === '',
  });

  const { data, client, loading, fetchMore } = useQuery(GET_MESSAGES, {
    variables: {
      cursor: null,
      limit: chatsLoadOnQuery,
      room: activeRoomId,
    },
    skip: activeRoomId === '',
    fetchPolicy: !roomsState.current.get(activeRoomId) ? 'network-only' : 'cache-first',
    onCompleted(res) {
      if (!res?.chatQuery) return;
      const room =
        roomsState.current.get(activeRoomId) ||
        roomsState.current
          .set(activeRoomId, {
            loadedAllChats: false,
            cursor: null,
            scroll: null,
            containerHeight: 0,
          })
          .get(activeRoomId);
      if (res.chatQuery.messages.length < chatsLoadOnQuery) room.loadedAllChats = true;
      room.cursor = res.chatQuery.cursor;
    },
  });

  useSubscription(CHATS_SUBSCRIPTION, {
    variables: { username, clientId: USER_ID, rooms: roomIds },
    skip: activeRoomId === null,
    onSubscriptionData({ client, subscriptionData: { data } }) {
      const { room, ...msg } = data.chatAdded;
      const roomExists = rooms.find((el) => el.id === room);
      if (msg.username === username) {
        if (roomExists?.participant) dispatch(setUnreadRoom({ room, unreadCount: 0 }));
      } else if (room !== activeRoomId) {
        dispatch(setUnreadRoom({ room, unreadCount: unreadRooms[room] ? unreadRooms[room] + 1 : 1 }));
      } else if (participant.current) {
        unreadChatMutation({
          variables: { room: activeRoomId, participant: participant.current, clientId: USER_ID },
        });
      }
      if (!roomExists) {
        const tabIsOpen = rooms.some((el) => el.participant === msg.username);
        if (isMounted()) {
          joinRoomMutation({
            variables: {
              room,
              leave: false,
              clientId: USER_ID,
            },
          });
        }
        dispatch(changeRooms({ room: { participant: msg.username, id: room }, ...(tabIsOpen && { change: true }) }));
      }
      const queryInfo = {
        query: GET_MESSAGES,
        variables: { room },
      };
      client.writeQuery({
        ...queryInfo,
        data: {
          chatQuery: {
            __typename: 'ChatResponse',
            cursor: '',
            messages: msg,
          },
        },
      });
      chatStatus.current.layoutChanged = true;
    },
  });

  useSubscription(CHANGE_CHAT_SUBSCRIPTION, {
    variables: { username, clientId: USER_ID },
    skip: activeRoomId === null,
    onSubscriptionData({
      subscriptionData: {
        data: { changeChat },
      },
    }) {
      if (changeChat.participant) {
        const name = changeChat.prevRoom || changeChat.room;
        const room = rooms.find((el) => el.id === name);
        if (!room) return;
        const isActiveRoom = activeRoomId === changeChat.room || activeRoomId === room.id;
        if (changeChat.prevRoom) {
          dispatch(changeRooms({ room: { id: room.id }, leave: true }));
          dispatch(changeRooms({ room: { id: changeChat.room, participant: changeChat.participant } }));
          client.cache.evict({
            id: 'ROOT_QUERY',
            fieldName: 'chatQuery',
            args: { room: changeChat.prevRoom },
          });
          if (unreadRooms[changeChat.prevRoom]) {
            dispatch(setUnreadRoom({ room: changeChat.room, unreadCount: unreadRooms[changeChat.prevRoom] }));
          }
        } else {
          dispatch(
            changeRooms({
              room: { id: changeChat.room, participant: changeChat.participant },
              rename: true,
            }),
          );
        }
        client.cache.evict({
          id: 'ROOT_QUERY',
          fieldName: 'chatQuery',
          args: { room: changeChat.room },
        });
        client.cache.gc();
        if (isActiveRoom) {
          handleActiveRoom(changeChat.participant, changeChat.room, true, true);
        }
      } else if (unreadRooms[changeChat.room] >= 0) {
        dispatch(setUnreadRoom({ room: changeChat.room, unreadCount: null }));
      }
    },
  });

  const fetchChats = useCallback(
    async (limit, roomId, room) => {
      try {
        chatStatus.current.loadingOnScroll = true;
        room.containerHeight = contRef.current.scrollHeight;
        const {
          data: {
            chatQuery: { messages, cursor },
          },
        } = await fetchMore({
          variables: {
            cursor: room?.cursor,
            limit,
            room: roomId,
          },
        });
        room.cursor = cursor;
        if (messages.length < limit) room.loadedAllChats = true;
        if (messages.length > 0) chatStatus.current.loadedOnScroll = true;
      } catch (err) {
        console.log(err);
      } finally {
        if (isMounted()) {
          setShowLoadOnScroll(false);
          chatStatus.current.loadingOnScroll = false;
          chatStatus.current.layoutChanged = true;
        }
      }
    },
    [fetchMore, isMounted],
  );

  const loadOnScroll = useCallback(() => {
    if (!contRef.current || !chatStatus.current.chatsFetched) return;
    const { scrollTop, scrollHeight, offsetHeight } = contRef.current;
    const room = roomsState.current.get(chatStatus.current.activeRoomIdRef);
    if (!room || room.loadedAllChats || scrollHeight < offsetHeight + 10) return;

    if (!chatStatus.current.loadingOnScroll && scrollTop <= loadOnScrollOffset) {
      fetchChats(chatsLoadOnScroll, chatStatus.current.activeRoomIdRef, room);
    }

    if (chatStatus.current.loadingOnScroll) {
      if (scrollTop > loadingHeight) {
        setShowLoadOnScroll(false);
      } else if (scrollTop <= loadingHeight) {
        setShowLoadOnScroll(true);
      }
    }
  }, [fetchChats]);

  const scrollToBottom = useCallback((anim) => {
    if (!contRef.current) return;
    const { scrollTop, scrollHeight, offsetHeight } = contRef.current;
    const start = scrollTop;
    const end = scrollHeight - offsetHeight;
    chatStatus.current.messagesScroll = end;
    if (anim) {
      chatStatus.current.animRef.restart(
        (progress) => (contRef.current.scrollTop = (end - start) * progress + start),
        false,
      );
    } else {
      chatStatus.current.animRef.pause();
      contRef.current.scrollTop = end;
      chatStatus.current.layoutChanged = false;
    }
  }, []);

  const handleSubmit = useCallback(
    async (message, cb) => {
      if (chatStatus.current.submiting || !message || !isMounted() || (!activeRoomId && !participant.current)) return;
      cb();
      const date = new Date().toJSON();
      const msg = { message, date, id: nanoid(11), clientId: USER_ID, participant: participant.current };
      chatStatus.current.submiting = true;
      chatStatus.current.isTyping = false;
      const { scrollTop, scrollHeight, offsetHeight } = contRef.current;
      if (scrollHeight - scrollTop - offsetHeight < smoothScrollOffset) {
        msgAddingSet(true);
        chatStatus.current.layoutChanged = true;
      }
      try {
        if (!activeRoomId) {
          const {
            data: { createRoom },
          } = await createRoomMutation({
            variables: {
              participant: participant.current,
              clientId: USER_ID,
            },
          });
          msg.room = createRoom.id;
          dispatch(changeRooms({ room: { id: createRoom.id, participant: createRoom.participant }, change: true }));
          handleActiveRoom(createRoom.participant, createRoom.id, true);
        } else {
          msg.room = activeRoomId;
        }
        const [{ data }] = await Promise.all([
          addMessageMutation({ variables: msg }),
          activeRoomId &&
            userTypingMutation({
              variables: {
                clientId: USER_ID,
                room: activeRoomId,
                typing: false,
              },
            }),
        ]);
        if (!isMounted()) return;
        const { room, ...rest } = data.addMessage;
        const queryInfo = {
          query: GET_MESSAGES,
          variables: {
            room,
          },
        };
        const { chatQuery } = client.readQuery(queryInfo);
        client.writeQuery({
          ...queryInfo,
          data: {
            chatQuery: {
              ...chatQuery,
              messages: rest,
            },
          },
        });
        chatStatus.current.layoutChanged = true;
        if (participant.current) {
          dispatch(setUnreadRoom({ room, unreadCount: 0 }));
        }
      } catch (err) {
        console.log(err);
      } finally {
        if (isMounted()) {
          chatStatus.current.submiting = false;
          msgAddingSet(false);
        }
      }
    },
    [
      addMessageMutation,
      userTypingMutation,
      createRoomMutation,
      client,
      activeRoomId,
      handleActiveRoom,
      participant,
      dispatch,
      isMounted,
    ],
  );

  const handleRoomSearch = useCallback(
    (e) => {
      const value = fixStringForRegexp(e.target.value);
      const trimmedValue = value.trim();
      const len = trimmedValue.length;
      if (len > 1) {
        let i = 1;
        while (i < len) {
          const queryInfo = {
            query: FIND_ROOM_QUERY,
            variables: {
              room: trimmedValue.slice(0, len - i),
            },
          };
          const data = client.readQuery(queryInfo);
          if (data && data.findRoom.length > 0) break;
          if (data && data.findRoom.length === 0) return;
          i++;
        }
      }
      throttledSetRoomSearch.current(trimmedValue);
    },
    [client],
  );

  const handleRoomClose = useCallback(
    async (id, room, participant) => {
      function nextActiveRoom() {
        const ind = rooms.findIndex((el) => (room ? el.room === room : el.participant === participant));
        const nextInd = ind === 0 ? 1 : ind - 1;
        const nextId = rooms[nextInd].id;
        const nextName = rooms[nextInd].room || rooms[nextInd].participant;
        handleActiveRoom(nextName, nextId, !!rooms[nextInd].participant, true);
      }

      const isActiveRoom = (room || participant) === activeRoom;
      if (isActiveRoom) nextActiveRoom();

      if (!id) {
        dispatch(changeRooms({ room: { participant, room }, leave: true }));
        return;
      }

      dispatch(changeRooms({ room: { id }, leave: true }));
      dispatch(setUnreadRoom({ room: id, unreadCount: null }));
      joinRoomMutation({
        variables: {
          room: id,
          leave: true,
          clientId: USER_ID,
        },
      }).catch((err) => console.log(err));
    },
    [rooms, dispatch, joinRoomMutation, handleActiveRoom, activeRoom],
  );

  const moveTabs = useCallback((to) => {
    const style = tabsRef.current.style.transform;
    const from = style ? +style.slice(11, -3) : 0;
    tabsRef.current.style.transform = `translateX(${to + from}px)`;
  }, []);

  const setArrowsVisibility = useCallback(() => {
    const contRect = tabsContRef.current.getBoundingClientRect();
    const tabsRect = tabsRef.current.getBoundingClientRect();
    if (tabsRect.width - contRect.width > 1) {
      if (Math.abs(tabsRect.left - contRect.left) < 1) {
        setArrows('left');
      } else if (Math.abs(tabsRect.right - contRect.right) < 1) {
        setArrows('right');
      } else {
        setArrows('');
      }
    } else {
      if (Math.abs(tabsRect.left - contRect.left) > 1) {
        moveTabs(contRect.left - tabsRect.left);
      }
      setArrows('none');
    }
  }, [moveTabs]);

  const handleArrowClick = useCallback(
    (e) => {
      const { direction } = e.target.dataset;
      if (direction === arrows || chatStatus.current.isAnimating) return;
      chatStatus.current.isAnimating = true;
      const contRect = tabsContRef.current.getBoundingClientRect();
      const tabsRect = tabsRef.current.getBoundingClientRect();
      const diff = Math.ceil(Math.abs(contRect[direction] - tabsRect[direction]));
      const step = Math.min(diff, arrowAnimationDistance);
      const style = tabsRef.current.style.transform;
      const from = style ? +style.slice(11, -3) : 0;
      const to = from + (direction === 'left' ? step : -step);
      const isScrolled = diff <= arrowAnimationDistance;
      if (isScrolled) {
        setArrows(direction);
      } else {
        setArrows('');
      }
      animate({
        duration: arrowAnimationTime,
        timing: isScrolled ? Back.Out : Circular.Out,
        draw(progress) {
          if (!isMounted()) return;
          const distance = (to - from) * progress + from;
          tabsRef.current.style.transform = `translateX(${distance}px)`;
        },
        cb() {
          chatStatus.current.isAnimating = false;
        },
      });
    },
    [arrows, isMounted],
  );

  const handleTabsFocus = useCallback(
    (e) => {
      const contBox = tabsContRef.current.getBoundingClientRect();
      const tabsBox = tabsRef.current.getBoundingClientRect();
      if (tabsBox.left > contBox.left) {
        moveTabs(Math.ceil(contBox.left - tabsBox.left));
        return setArrowsVisibility();
      } else if (tabsBox.right < contBox.right) {
        moveTabs(Math.ceil(contBox.right - tabsBox.right));
        return setArrowsVisibility();
      }

      const targetBox = e?.target?.getBoundingClientRect() || e.getBoundingClientRect();
      let diff;
      if (targetBox.left < contBox.left) {
        diff = Math.ceil(contBox.left - targetBox.left);
      } else if (targetBox.right > contBox.right) {
        diff = Math.ceil(contBox.right - targetBox.right);
      }
      moveTabs(diff);
      setArrowsVisibility();
    },
    [setArrowsVisibility, moveTabs],
  );

  const addNewRoom = useCallback(
    (room, id) => {
      dispatch(changeRooms({ room: { room, id } }));
      handleActiveRoom(room, id);
    },
    [dispatch, handleActiveRoom],
  );

  const handleJoinRoom = useCallback(
    async (room) => {
      try {
        if (chatStatus.current.joiningRoom) return;
        const roomExists = rooms.find((el) => (el.room || el.participant).toUpperCase() === room.toUpperCase());
        throttledSetRoomSearch.current('');
        searchRef.current.children[0].value = '';
        if (roomExists) {
          handleActiveRoom(roomExists.room || roomExists.participant, roomExists.id, !!roomExists.participant, true);
          return;
        }
        chatStatus.current.joiningRoom = true;
        const {
          data: { joinRoom },
        } = await joinRoomMutation({
          variables: {
            room,
            leave: false,
            clientId: USER_ID,
            byRoomName: true,
          },
        });
        if (joinRoom?.id && isMounted()) addNewRoom(room, joinRoom.id);
      } catch (err) {
        console.log(err);
      } finally {
        chatStatus.current.joiningRoom = false;
      }
    },
    [joinRoomMutation, addNewRoom, handleActiveRoom, rooms, searchRef, isMounted],
  );

  const handleCreateRoom = useCallback(async () => {
    if (chatStatus.current.roomIsCreating === 'creating') return;
    if (roomQueryLoad) {
      chatStatus.current.roomIsCreating = 'pending';
      return;
    }
    function handleRoomError(msg, ms) {
      clearTimeout(chatStatus.current.errorTimer);
      createRoomErrorSet(msg);
      chatStatus.current.errorTimer = setTimeout(() => createRoomErrorSet(''), ms);
    }
    chatStatus.current.roomIsCreating = 'creating';
    const inputValue = fixStringForRegexp(searchRef.current.children[0].value).trim();
    if (inputValue.length < 3) {
      chatStatus.current.roomIsCreating = '';
      handleRoomError('Room should be at least 3 characters long', 2000);
      return;
    }
    throttledSetRoomSearch.current('');
    searchRef.current.children[0].value = '';
    if (inputValue.toUpperCase() === mainChatName.toUpperCase()) {
      return handleJoinRoom(inputValue);
    }
    const room = roomQueryData?.findRoom.find((el) => el.toUpperCase() === inputValue.toUpperCase());
    if (room) {
      handleJoinRoom(room);
    } else {
      try {
        const {
          data: { createRoom },
        } = await createRoomMutation({
          variables: {
            room: inputValue,
            clientId: USER_ID,
          },
        });
        addNewRoom(createRoom.room, createRoom.id);
        let i = 1;
        while (i <= createRoom.room.length) {
          const queryInfo = {
            query: FIND_ROOM_QUERY,
            variables: {
              room: createRoom.room.slice(0, i),
            },
          };
          const data = client.readQuery(queryInfo);
          if (data) {
            client.writeQuery({
              ...queryInfo,
              data: {
                findRoom: [...data.findRoom, createRoom.room],
              },
            });
          }
          i++;
        }
      } catch (err) {
        handleRoomError(err.message, 4000);
      } finally {
        chatStatus.current.roomIsCreating = '';
      }
    }
  }, [roomQueryLoad, roomQueryData, handleJoinRoom, createRoomMutation, addNewRoom, client, searchRef]);

  const handleKeyPress = useCallback(
    (e) => {
      if (e.key !== 'Enter' || e.shiftKey) return;
      handleCreateRoom();
    },
    [handleCreateRoom],
  );

  const handleInputChange = useCallback(
    (val) => {
      const length = val.length;
      if (
        (chatStatus.current.isTyping && length) ||
        (!chatStatus.current.isTyping && !length) ||
        !chatStatus.current.activeRoomIdRef
      ) {
        return;
      }
      chatStatus.current.isTyping = !!length;
      userTypingMutation({
        variables: {
          clientId: USER_ID,
          room: chatStatus.current.activeRoomIdRef,
          typing: chatStatus.current.isTyping,
        },
      }).catch((err) => console.log(err));
    },
    [userTypingMutation],
  );

  if (activeRoomId !== chatStatus.current.activeRoomIdRef) {
    chatStatus.current.chatsFetched = false;
    if (showLoadOnScroll) {
      setShowLoadOnScroll(false);
    }
    const room = roomsState.current.get(chatStatus.current.activeRoomIdRef);
    if (room) {
      room.containerHeight = contRef.current.scrollHeight;
      room.scroll = contRef.current.scrollTop;
    }
  }

  useEffect(() => {
    if (showLoadOnScroll) {
      chatStatus.current.layoutChanged = true;
      contRef.current.scrollTop = contRef.current.scrollTop - loadingHeight;
      roomsState.current.get(chatStatus.current.activeRoomIdRef).containerHeight = contRef.current.scrollHeight;
    }
  }, [showLoadOnScroll]);

  useEffect(() => {
    if (chatStatus.current.roomIsCreating === 'pending' && !roomQueryLoad) {
      handleCreateRoom();
    }
  }, [roomQueryLoad, roomQueryData, handleCreateRoom]);

  useEffect(() => {
    async function loadEmojiData() {
      const data = await import(/* webpackChunkName: 'apple-emoji' */ 'emoji-mart/data/apple.json');
      if (isMounted()) {
        emojiDataSet(data.default);
        emojiIndex.current = new NimbleEmojiIndex(data.default);
      }
    }

    loadEmojiData();

    if (!messagesRef.current || !contRef.current) return;

    const cont = contRef.current;
    const throttledLoad = throttle(loadOnScroll, 100);
    cont.addEventListener('scroll', throttledLoad, { passive: true });

    const newMsgScrollOffset = msgAddingHeight + smoothScrollOffset;

    const observer = new ResizeObserver(() => {
      if (!cont) return;
      const { scrollTop, scrollHeight, offsetHeight } = cont;
      const room = roomsState.current.get(chatStatus.current.activeRoomIdRef);
      const scrollIsSaved = Number.isFinite(room?.scroll);
      if (scrollHeight < offsetHeight + 10) {
        if (scrollIsSaved) {
          room.scroll = null;
          room.containerHeight = 0;
        }
        return;
      }
      const fullHeight = scrollTop + offsetHeight;
      if (scrollIsSaved) {
        const chatsLoadedAfterChange =
          room.scroll < smoothScrollOffset && !room.loadedAllChats && room.containerHeight < scrollHeight;
        let scroll;
        if (room.containerHeight <= scrollHeight || chatsLoadedAfterChange) {
          scroll = chatsLoadedAfterChange ? scrollHeight - room.containerHeight : room.scroll;
          room.scroll = null;
          room.containerHeight = 0;
        } else {
          scroll =
            room.scroll <= scrollHeight ? room.scroll : Math.trunc((room.scroll * scrollHeight) / room.containerHeight);
        }
        chatStatus.current.chatsFetched = true;
        chatStatus.current.layoutChanged = false;
        cont.scrollTop = scroll;
        chatStatus.current.messagesScroll = scroll;
      } else if (!chatStatus.current.chatsFetched) {
        chatStatus.current.chatsFetched = true;
        scrollToBottom(false);
      } else if (chatStatus.current.loadedOnScroll) {
        chatStatus.current.loadedOnScroll = false;
        if (scrollTop <= loadOnScrollOffset) {
          cont.scrollTop = scrollHeight + scrollTop - (room?.containerHeight || 0);
        }
      } else if (
        chatStatus.current.layoutChanged &&
        (scrollHeight - fullHeight < smoothScrollOffset ||
          (chatStatus.current.messagesHeight - fullHeight >= -loadingHeight &&
            chatStatus.current.messagesHeight - fullHeight <= newMsgScrollOffset))
      ) {
        scrollToBottom(true);
      } else {
        chatStatus.current.layoutChanged = false;
      }
      chatStatus.current.messagesHeight = scrollHeight;
    });

    observer.observe(messagesRef.current);

    const chatRef = chatStatus.current;

    return () => {
      observer.disconnect();
      client.stop();
      client.cache.reset();
      subscriptionClient.dispose();
      cont.removeEventListener('scroll', throttledLoad);
      clearTimeout(chatRef.errorTimer);
    };
  }, [scrollToBottom, isMounted, client, loadOnScroll]);

  useLayoutEffect(() => {
    if (activeRoomId === chatStatus.current.activeRoomIdRef) return;
    chatStatus.current.activeRoomIdRef = activeRoomId;
    if (unreadRooms[activeRoomId]) {
      const room = roomsState.current.get(activeRoomId);
      scrollToBottom(false);
      if (participant.current) {
        unreadChatMutation({ variables: { room: activeRoomId, participant: participant.current, clientId: USER_ID } });
      }
      if (Number.isFinite(room?.scroll)) {
        room.scroll = null;
        room.containerHeight = 0;
      }
      dispatch(setUnreadRoom({ room: activeRoomId, unreadCount: null }));
    }
  }, [activeRoomId, unreadRooms, dispatch, scrollToBottom, unreadChatMutation, participant]);

  useLayoutEffect(() => {
    if (rooms[0].id == null) return;
    const unreadLen = Object.keys(unreadRooms).length;
    if (
      (rooms.length === chatStatus.current.roomsLen[0] && unreadLen === chatStatus.current.roomsLen[1]) ||
      chatStatus.current.roomsLen[0] === 1 ||
      (rooms.length - 1 === chatStatus.current.roomsLen[0] &&
        rooms[rooms.length - 1].participant &&
        unreadRooms[rooms[rooms.length - 1].id] > 0)
    ) {
      setArrowsVisibility();
      chatStatus.current.roomsLen = [rooms.length, unreadLen];
      return;
    }
    const contBox = tabsContRef.current.getBoundingClientRect();
    const tabBox = tabsRef.current.getBoundingClientRect();
    const rightDiff = tabBox.right - contBox.right;
    const leftDiff = contBox.left - tabBox.left;
    if (
      (rightDiff > 1 && rooms.length > chatStatus.current.roomsLen[0]) ||
      (rightDiff < 1 && rooms.length === chatStatus.current.roomsLen[0])
    ) {
      moveTabs(-rightDiff);
    } else if (leftDiff > 1 && rooms.length < chatStatus.current.roomsLen[0]) {
      moveTabs(Math.abs(rightDiff) < leftDiff ? Math.abs(rightDiff) : leftDiff);
    }
    setArrowsVisibility();
    chatStatus.current.roomsLen = [rooms.length, unreadLen];
  }, [setArrowsVisibility, moveTabs, rooms, unreadRooms]);

  useLayoutEffect(() => {
    if (!isMounted()) return;
    if (restoreScroll) {
      chatStatus.current.messagesScroll = contRef.current.scrollTop;
    } else {
      contRef.current.scrollTop = chatStatus.current.messagesScroll;
    }
  }, [restoreScroll, isMounted]);

  useLayoutEffect(() => {
    if (!isMounted() && typingUsers.length === 0) return;
    if (typingUsers.length > 0) chatStatus.current.layoutChanged = true;
    const { scrollHeight, scrollTop, offsetHeight } = contRef.current;
    if (
      typingUsers.length === 0 &&
      chatStatus.current.chatsFetched &&
      scrollHeight - scrollTop - offsetHeight < smoothScrollOffset &&
      scrollTop + offsetHeight >= scrollHeight
    ) {
      contRef.current.scrollTop = scrollTop - 1;
      contRef.current.scrollTop = scrollTop;
    }
  }, [typingUsers, scrollToBottom, isMounted]);

  return (
    <>
      <div className="chat-room-cont" ref={animRef}>
        <div className="chat-room">
          {arrows !== 'none' && (
            <div
              className={`chat-room-arrow chat-room-arrow--left ${arrows === 'left' ? 'is-hidden' : ''}`}
              data-direction="left"
              onClick={handleArrowClick}
            />
          )}
          {arrows !== 'none' && (
            <div
              className={`chat-room-arrow chat-room-arrow--right ${arrows === 'right' ? 'is-hidden' : ''}`}
              data-direction="right"
              onClick={handleArrowClick}
            />
          )}
          <div className="chat-room-tabs-cont" ref={tabsContRef}>
            <div className="chat-room-tabs" ref={tabsRef} onFocus={handleTabsFocus}>
              {rooms.map((el) => {
                const name = el.room || el.participant;
                if (!name) return null;
                const isActive = name.toUpperCase() === activeRoom.toUpperCase();
                const unreadMsgCount = unreadRooms[el.id];
                return (
                  <ChatTab
                    key={name}
                    name={name}
                    id={el.id}
                    room={el.room}
                    participant={el.participant}
                    handleActiveRoom={handleActiveRoom}
                    handleRoomClose={handleRoomClose}
                    isActive={isActive}
                    unreadMsgCount={unreadMsgCount}
                  />
                );
              })}
            </div>
          </div>
          <div className="chat-room-messages-cont" ref={contRef}>
            {((loading && !chatStatus.current.chatsFetched) || showLoadOnScroll) && (
              <div className="loading-cont">
                <div className="loading"></div>
                <div className="loading"></div>
                <div className="loading"></div>
              </div>
            )}
            <div className="chat-room-messages" ref={messagesRef}>
              {data?.chatQuery?.messages.length > 0 && (
                <>
                  <ChatList
                    chats={data.chatQuery.messages}
                    username={username}
                    emojiIndex={emojiIndex}
                    data={emojiData}
                    handleUsersClick={handleUsersClick}
                  />
                  {msgAdding && <div className="chat-room-messages__loading" />}
                  {typingUsers.length > 0 && (
                    <div className="chat-room-messages__message-cont">{`${typingUsers.join(', ')} is typing...`}</div>
                  )}
                </>
              )}
            </div>
          </div>
          <ChatInput
            onSubmit={handleSubmit}
            onChange={handleInputChange}
            emojiIndex={emojiIndex}
            data={emojiData}
            activeRoom={activeRoom}
          />
        </div>
      </div>
      <div className="chat-room-search" ref={searchRef}>
        <input
          type="search"
          className="chat-room-search__input"
          placeholder="Join/create room"
          onChange={handleRoomSearch}
          onKeyPress={handleKeyPress}
        />
        <button className="chat-room-search__submit" aria-label="Search" onClick={handleCreateRoom}>
          <svg viewBox="0 0 500 500">
            <polygon
              points="455,212.5 242.5,212.5 242.5,0 212.5,0 212.5,212.5 0,212.5 0,242.5
              212.5,242.5 212.5,455 242.5,455 242.5,242.5 455,242.5"
            />
          </svg>
        </button>
        <ul className="chat-room-search__list">
          {roomSearch !== '' &&
            roomQueryData?.findRoom.map((room) => (
              <li className="chat-room-search__item" key={room} onClick={() => handleJoinRoom(room)}>
                {room}
              </li>
            ))}
        </ul>
        {createRoomError && <p className="chat-room-search__error">{createRoomError}</p>}
      </div>
    </>
  );
};

ChatRoom.propTypes = {
  animRef: PropTypes.object.isRequired,
  searchRef: PropTypes.object.isRequired,
  typingUsers: PropTypes.array.isRequired,
  restoreScroll: PropTypes.bool.isRequired,
  username: PropTypes.string,
  activeRoom: PropTypes.string,
  activeRoomId: PropTypes.string,
  roomIds: PropTypes.array,
  rooms: PropTypes.array.isRequired,
  handleActiveRoom: PropTypes.func.isRequired,
  handleUsersClick: PropTypes.func.isRequired,
  participant: PropTypes.object,
};

export default memo(ChatRoom);
