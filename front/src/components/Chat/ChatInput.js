import { useState, useEffect, useRef, memo } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { toggleModal } from '../../redux/modalReducer';
import Modal from '../Modal/Modal';
import Canvas from '../Modal/Canvas';
import { getEmojiDataFromNative } from 'emoji-mart/dist-es/utils';
import NimblePicker from 'emoji-mart/dist-es/components/picker/nimble-picker';
import { drawingName } from '../../config';
import { urlReg, nativeReg, colonsReg, emoticonsReg } from '../../helpers/regexp';
import emojiSource from '../../assets/emoji-datasource-apple-32.png';

const emojiReg = new RegExp(
  `(${urlReg.source})|(${nativeReg.source})|(${colonsReg.source})|(${emoticonsReg.source})`,
  'gu',
);

function ChatInput({ onSubmit, onChange, emojiIndex, data, activeRoom }) {
  const [showEmoji, showEmojiSet] = useState(false);
  const dispatch = useDispatch();
  const canvas = useSelector((state) => state.modal.canvas);
  const inputRef = useRef(null);
  const canvasDrawing = useRef([]);
  const actions = useRef({
    index: 0,
    moves: [
      {
        html: '',
        offset: 0,
        el: -1,
      },
    ],
  });

  function openCanvas() {
    dispatch(toggleModal({ visibility: true, canvas: true }));
  }

  function toggleEmoji() {
    showEmojiSet((val) => !val);
  }

  function scrollInput(range) {
    if (inputRef.current.scrollHeight <= inputRef.current.offsetHeight) return;
    const span = document.createElement('span');
    range.insertNode(span);
    const { top, bottom } = span.getBoundingClientRect();
    const { top: contTop, bottom: contBot } = inputRef.current.getBoundingClientRect();
    const scroll = inputRef.current.scrollTop;
    if (bottom > contBot + 2) {
      inputRef.current.scrollTop = bottom - contBot + scroll;
    } else if (top < contTop) {
      inputRef.current.scrollTop = top - contTop + scroll;
    }
    inputRef.current.removeChild(span);
  }

  function saveActions(node, focusIndex) {
    actions.current.index += 1;
    if (actions.current.moves.length - 1 >= actions.current.index) {
      actions.current.moves.splice(actions.current.index, actions.current.moves.length - actions.current.index);
    }
    if (node === undefined) {
      const { focusNode, focusOffset } = window.getSelection();
      node = focusNode === inputRef.current ? -1 : [...inputRef.current.childNodes].indexOf(focusNode);
      focusIndex = focusOffset;
    }
    actions.current.moves.push({
      html: inputRef.current.innerHTML,
      el: node,
      offset: focusIndex,
    });
  }

  function createEmoji(colons, unified) {
    const img = document.createElement('img');
    img.className = 'emoji-cont';
    img.width = '22';
    img.height = '22';
    img.alt = colons;
    img.src = `https://cdnjs.cloudflare.com/ajax/libs/emoji-datasource-apple/6.0.1/img/apple/64/${unified}.png`;
    return img;
  }

  function insertInCaret(range) {
    const shouldInsert =
      range.endContainer !== inputRef.current &&
      ((range.endContainer.nodeType === 3 && range.endOffset < range.endContainer.textContent.length) ||
        [...inputRef.current.childNodes].indexOf(range.endContainer) < inputRef.current.childNodes.length - 1);
    const insertNode =
      (range.endContainer !== inputRef.current && range.endContainer.nextSibling) ||
      (range.endContainer === inputRef.current && inputRef.current.childNodes[range.endOffset]) ||
      null;
    return [shouldInsert, insertNode];
  }

  function onEmojiClick(emoji) {
    const sel = window.getSelection();
    let range;
    if (sel.anchorNode !== inputRef.current && !inputRef.current.contains(sel.anchorNode)) {
      range = document.createRange();
      let node = inputRef.current.childNodes[inputRef.current.childNodes.length - 1];
      let ind;
      if (!node || node.nodeType === 1) {
        node = inputRef.current;
        ind = inputRef.current.childNodes.length;
      } else {
        ind = Math.max(node.textContent.length - 1, 0);
      }
      range.setEnd(node, ind);
      range.collapse(false);
      sel.addRange(range);
    } else {
      range = sel.getRangeAt(0);
      range.deleteContents();
    }
    const img = createEmoji(emoji.colons, emoji.unified);
    const [insertInRange, insertNode] = insertInCaret(range);
    insertInRange ? range.insertNode(img) : inputRef.current.insertBefore(img, insertNode);
    inputRef.current.focus();
    const focusIndex = [...inputRef.current.childNodes].indexOf(img) + 1;
    sel.collapse(inputRef.current, focusIndex);
    saveActions(-1, focusIndex);
  }

  function handleCanvasSubmit() {
    dispatch(toggleModal({ visibility: false }));
    inputRef.current.focus();
    if (inputRef.current.querySelector('.drawing')) return;
    const sel = window.getSelection();
    const { focusNode, focusOffset } = sel;
    const range = sel.getRangeAt(0);
    if (!range.collapsed) {
      range.deleteContents();
    } else if (focusNode && focusNode !== inputRef.current) {
      if (focusOffset === 0) {
        const prev = focusNode.previousSibling;
        if (prev) {
          range.setStart(prev, prev.textContent.length > 0 ? prev.textContent.length - 1 : 0);
        }
      } else {
        range.setStart(focusNode, focusOffset - 1);
      }
      const text = range.toString();
      if (!/\s/.test(text)) {
        range.collapse(false);
        range.insertNode(document.createTextNode(' '));
      }
      range.collapse(false);
    }
    const canvasNode = document.createElement('span');
    canvasNode.setAttribute('contenteditable', false);
    canvasNode.className = 'drawing';
    canvasNode.textContent = drawingName + ' ';
    range.insertNode(canvasNode);
    range.collapse(false);
    saveActions();
    scrollInput(range);
  }

  function parseText(text) {
    let lastIndex = -1;
    const result = [];

    text.replace(emojiReg, (match, url, native, colons, emoticons, index, input) => {
      if (index > lastIndex && index > 0) {
        result.push(document.createTextNode(input.substring(lastIndex, index)));
      }

      if (url) {
        result.push(document.createTextNode(url));
      } else if (native && data) {
        const nativeEmoji = getEmojiDataFromNative(native, 'apple', data);
        result.push(createEmoji(nativeEmoji.colons, nativeEmoji.unified));
      } else if (colons) {
        const len = colons.length - 1 - (colons.includes('skin-tone') ? 13 : 0);
        const id = colons.slice(1, len);
        const colonsEmoji = emojiIndex.current.search(id)[0];
        result.push(
          colonsEmoji ? createEmoji(colonsEmoji.colons, colonsEmoji.unified) : document.createTextNode(colons),
        );
      } else if (emoticons) {
        const emoticonEmoji = emojiIndex.current.search(emoticons)[0];
        result.push(emoticonEmoji ? createEmoji(emoticons, emoticonEmoji.unified) : document.createTextNode(emoticons));
        emoticonEmoji && /^:.+\s$/.test(emoticons) && result.push(document.createTextNode('\u00A0'));
      }

      lastIndex = index + match.length;
    });

    if (lastIndex !== -1 && lastIndex < text.length) {
      result.push(document.createTextNode(text.slice(lastIndex).replace(/ $/, '\u00A0')));
    }

    return result;
  }

  function submitCb() {
    inputRef.current.innerHTML = '';
    inputRef.current.focus();
    actions.current.index = 0;
    actions.current.moves.splice(1, actions.current.moves.length - 1);
  }

  function handleSubmit() {
    if (inputRef.current.childNodes.length === 0) return;
    let text = '';
    [...inputRef.current.childNodes].forEach((el) => {
      if (el.nodeType === 1) {
        if (el.alt) {
          text += el.alt;
        } else if (canvasDrawing.current.length > 0 && el.textContent.includes(drawingName)) {
          text += el.textContent.replace(drawingName, ` ${canvasDrawing.current[canvasDrawing.current.length - 1]} `);
          canvasDrawing.current.length = 0;
        }
      } else {
        text += el.textContent;
      }
    });
    text = text.trim();
    if (text.length === 0) return;
    onSubmit(text, submitCb);
  }

  function handleKeyPress(e) {
    if (e.key === 'Enter' && !e.shiftKey) {
      e.preventDefault();
      handleSubmit();
    }
  }

  function handlePaste(e) {
    const pasteText = e.clipboardData?.getData('text/plain') || window.clipboardData?.getData('text');
    e.preventDefault();
    const editedText = pasteText
      .replace(/ {2}/g, ' \u00A0')
      .replace(/^ /gm, '\u00A0')
      .replace(/\r\n|\n\r|\r/gm, '\n');
    const sel = window.getSelection();
    const range = sel.getRangeAt(0);
    range.deleteContents();
    const result = parseText(editedText);
    const [insertInRange, insertNode] = insertInCaret(range);
    let lastNode;
    if (result.length > 0) {
      lastNode = result[result.length - 1];
      insertInRange
        ? result.reverse().forEach((node) => range.insertNode(node))
        : result.forEach((node) => inputRef.current.insertBefore(node, insertNode));
    } else {
      lastNode = document.createTextNode(editedText);
      insertInRange ? range.insertNode(lastNode) : inputRef.current.insertBefore(lastNode, insertNode);
    }
    const isText = lastNode.nodeType === 3;
    range.setEnd(
      isText ? lastNode : inputRef.current,
      isText ? lastNode.textContent.length : [...inputRef.current.childNodes].indexOf(lastNode) + 1,
    );
    range.collapse(false);
    inputRef.current.normalize();
    saveActions();
    scrollInput(range);
  }

  function handleKeyDown(e) {
    if (!e.ctrlKey || (e.key !== 'z' && e.key !== 'y')) return;
    e.preventDefault();
    const undo = e.key === 'z';
    if ((undo && actions.current.index <= 0) || (!undo && actions.current.index >= actions.current.moves.length - 1)) {
      return;
    }
    const index = actions.current.index + (undo ? -1 : +1);
    actions.current.index = index;
    inputRef.current.innerHTML = actions.current.moves[index].html;
    window
      .getSelection()
      .collapse(
        actions.current.moves[index].el === -1
          ? inputRef.current
          : inputRef.current.childNodes[actions.current.moves[index].el],
        actions.current.moves[index].offset,
      );
    try {
      scrollInput(window.getSelection().getRangeAt(0));
    } catch (err) {
      console.log(err);
    }
  }

  function handleInput(e) {
    const type = e.nativeEvent.inputType;

    if (type === 'historyUndo' || type === 'historyRedo') {
      const html = inputRef.current.innerHTML;
      actions.current.index = html.length > 0 ? 1 : 0;
      actions.current.moves.splice(1, actions.current.moves.length - 2);
      html.length > 0 && actions.current.moves.push(html);
      return;
    }

    if (
      (type === 'deleteContentForward' || type === 'deleteContentBackward') &&
      inputRef.current.innerHTML.length === 0
    ) {
      actions.current.index = 0;
    }

    if (type === 'insertLineBreak' || (type === 'insertText' && e.nativeEvent.data === null)) {
      inputRef.current.normalize();
    }

    if (type === 'insertText') {
      if (
        RegExp(`(${nativeReg.source})|(${colonsReg.source})|(${emoticonsReg.source})`).test(
          inputRef.current.textContent,
        )
      ) {
        [...inputRef.current.childNodes].forEach((el) => {
          if (el.nodeType === 3) {
            const text = el.textContent;
            const res = parseText(text);
            if (res.length > 0) {
              if (!res.some((el) => el.nodeType === 1)) return;
              res.forEach((node) => inputRef.current.insertBefore(node, el));
              inputRef.current.removeChild(el);
              const newEl = res.find((el) => el.nodeType === 1);
              const nextSibl = newEl.nextSibling;
              window
                .getSelection()
                .collapse(
                  nextSibl ? nextSibl : inputRef.current,
                  nextSibl ? (nextSibl.textContent === '\u00A0' ? 1 : 0) : inputRef.current.childNodes.length,
                );
            }
          }
        });
      }
    }

    saveActions();
  }

  useEffect(() => {
    if (!showEmoji) return;
    const emoji = document.querySelector('.emoji-mart');
    function hideEmoji(e) {
      if (!e.path?.includes(emoji) || e.which === 27) showEmojiSet(false);
    }
    document.documentElement.addEventListener('click', hideEmoji);
    document.documentElement.addEventListener('keydown', hideEmoji);
    return () => {
      document.documentElement.removeEventListener('click', hideEmoji);
      document.documentElement.removeEventListener('keydown', hideEmoji);
    };
  }, [showEmoji]);

  useEffect(() => {
    const connection = navigator.connection?.effectiveType;
    const imgTO = setTimeout(() => {
      const img = new Image();
      img.src = emojiSource;
    }, 2000 * (connection === '4g' ? 1 : connection === '3g' ? 2 : 3));

    const options = { attributes: false, childList: true, subtree: false };
    const observer = new MutationObserver(() => {
      let text = '';
      [...inputRef.current.childNodes].forEach((el) => {
        if (el.nodeType === 1) {
          text += el.alt ? el.alt : el.className === 'drawing' ? drawingName : '';
        } else if (el.nodeType === 3) {
          text += el.textContent;
        }
      });
      onChange(text);
    });

    observer.observe(inputRef.current, options);

    return () => {
      clearTimeout(imgTO);
      observer.disconnect();
    };
  }, [onChange]);

  useEffect(() => {
    inputRef.current?.focus();
  }, [activeRoom]);

  return (
    <>
      {canvas && (
        <Modal>
          <Canvas canvasDrawing={canvasDrawing} onSubmit={handleCanvasSubmit} />
        </Modal>
      )}
      <div
        contentEditable
        className="chat-room__input"
        placeholder="Message..."
        ref={inputRef}
        autoComplete="off"
        spellCheck="false"
        onInput={handleInput}
        onKeyDown={handleKeyDown}
        onKeyPress={handleKeyPress}
        onPaste={handlePaste}
      />
      <button className="chat-room__smiley-btn" onClick={toggleEmoji} aria-label="Open emoji picker">
        <svg viewBox="0 0 122.88 122.88" xmlns="http://www.w3.org/2000/svg">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            fill="#fbd433"
            d="M45.54 2.11c32.77-8.78 66.45 10.67 75.23 43.43 8.78 32.77-10.67 66.45-43.43 75.23-32.77
              8.78-66.45-10.67-75.23-43.43-8.78-32.77 10.66-66.45 43.43-75.23z"
          />
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            fill="#141518"
            d="M45.78 31.71c4.3 0 7.78 6.6 7.78 14.75s-3.48 14.75-7.78 14.75S38 54.61 38 46.46c0-8.14 3.48-14.75
              7.78-14.75zM22.43 80.59c.42-7.93 4.53-11.46 11.83-11.76l-5.96 5.93c16.69 21.63 51.01 21.16 65.78-.04l
              -5.47-5.44c7.3.3 11.4 3.84 11.83 11.76l-3.96-3.93c-16.54 28.07-51.56 29.07-70.7.15l-3.35 3.33zM77.1 31.71
              c4.3 0 7.78 6.6 7.78 14.75s-3.49 14.75-7.78 14.75-7.78-6.6-7.78-14.75c-.01-8.14 3.48-14.75 7.78-14.75z"
          />
        </svg>
      </button>
      {showEmoji && data && (
        <NimblePicker
          set="apple"
          data={data}
          onSelect={onEmojiClick}
          perLine={7}
          emojiSize={28}
          sheetSize={32}
          backgroundImageFn={() => emojiSource}
          showSkinTones={false}
          showPreview={false}
          emojiTooltip
          exclude={['flags', 'symbols']}
        />
      )}
      <button className="chat-room__canvas-btn" onClick={openCanvas} aria-label="Open canvas">
        <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
          <path
            strokeWidth="2"
            strokeLinecap="round"
            fill="none"
            stroke="#000"
            strokeMiterlimit="10"
            strokeLinejoin="round"
            d="M11.3 26.5 4 28l1.5-7.3L21.6 4.5c.8-.8 2.1-.8 2.9 0l2.9 2.9c.8.8.8 2.1
              0 2.9L11.3 26.5zM18.7 7.5l5.8 5.8M18.7 13.3l-8.8 8.8"
          />
        </svg>
      </button>
      <button className="chat-room__button" onClick={handleSubmit}>
        Send
      </button>
    </>
  );
}

ChatInput.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  emojiIndex: PropTypes.object,
  data: PropTypes.object,
  activeRoom: PropTypes.string.isRequired,
};

export default memo(ChatInput);
