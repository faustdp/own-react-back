import PropTypes from 'prop-types';
import classNames from 'classnames';
import { mainChatName } from '../../config';

function ChatTab({ name, id, room, participant, handleActiveRoom, handleRoomClose, isActive, unreadMsgCount }) {
  const handleTabClick = () => {
    if (!isActive) handleActiveRoom(name, id, !!participant, true);
  };

  const handleCloseClick = (e) => {
    e.stopPropagation();
    handleRoomClose(id, room, participant);
  };

  return (
    <div className="chat-room-tabs__tab-cont" onClick={handleTabClick} title={name}>
      <button
        className={classNames({ 'chat-room-tabs__tab': true, 'is-active': isActive })}
        {...(isActive && { tabIndex: -1 })}
        aria-label="Close Tab"
      >
        {name}
        {unreadMsgCount > 0 ? (
          <span style={{ color: 'FireBrick' }}>{unreadMsgCount}</span>
        ) : unreadMsgCount === 0 ? (
          <span style={{ color: 'DodgerBlue' }}>•</span>
        ) : null}
      </button>
      {name !== mainChatName && <button className="chat-room-tabs__tab-close" onClick={handleCloseClick}></button>}
    </div>
  );
}

ChatTab.propTypes = {
  name: PropTypes.string.isRequired,
  handleActiveRoom: PropTypes.func.isRequired,
  handleRoomClose: PropTypes.func.isRequired,
  isActive: PropTypes.bool.isRequired,
  room: PropTypes.string,
  id: PropTypes.string,
  participant: PropTypes.string,
  unreadMsgCount: PropTypes.number,
};

export default ChatTab;
