import { Fragment, memo } from 'react';
import PropTypes from 'prop-types';

const ChatUsers = memo(function ChatUsers({ users, animRef, username, loading, handleUsersClick }) {
  const length = users.length;
  const lastIndex = length - 1;
  const sortedUsers = [...users].sort((a, b) =>
    a.username === username ? -1 : b.username === username ? 1 : a.username.localeCompare(b.username),
  );
  return (
    <div className="chat-users" ref={animRef}>
      <h2 className="chat-users__heading">
        Currently online {loading ? '?' : length} {length === 1 ? 'user' : 'users'}:
      </h2>
      <div className="chat-users__list">
        {loading ? (
          'Loading...'
        ) : length > 0 ? (
          <Fragment>
            {sortedUsers.map((user, i) => (
              <Fragment key={user.username}>
                {user.username === username ? (
                  <span>You</span>
                ) : (
                  <button onClick={() => handleUsersClick(user.username)}>{user.username}</button>
                )}
                {i !== lastIndex && ', '}
              </Fragment>
            ))}
          </Fragment>
        ) : null}
      </div>
    </div>
  );
});

ChatUsers.propTypes = {
  users: PropTypes.array.isRequired,
  animRef: PropTypes.object.isRequired,
  username: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  handleUsersClick: PropTypes.func.isRequired,
};

export default ChatUsers;
