import loadable from '@loadable/component';

const LoadableChat = loadable(() => import(/* webpackChunkName: 'chat' */ './Chat'), {
  fallback: null,
});

export default LoadableChat;
