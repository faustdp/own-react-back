import { useState, useEffect, memo, Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import timeDifference from '../../helpers/timeDifference';
import { getEmojiDataFromNative } from 'emoji-mart/dist-es/utils';
import NimbleEmoji from 'emoji-mart/dist-es/components/emoji/nimble-emoji';
import { dataImgReg, urlReg, nativeReg, colonsReg, emoticonsReg } from '../../helpers/regexp';
import emojiSource from '../../assets/emoji-datasource-apple-32.png';

const finalReg = new RegExp(
  `(${dataImgReg.source})|(${urlReg.source})|(${nativeReg.source})|(${colonsReg.source})|(${emoticonsReg.source.replace(
    '[^\\S\\r\\n]',
    '',
  )})`,
  'giu',
);

const MINUTE = 60 * 1000;

const ChatList = memo(function ChatList({ chats, username, emojiIndex, data, handleUsersClick }) {
  const [, setDate] = useState(null);

  const createEmoji = (id, colons) =>
    data ? (
      <NimbleEmoji
        emoji={id}
        size={26}
        data={data}
        set="apple"
        sheetSize={32}
        backgroundImageFn={() => emojiSource}
        children={colons}
        tooltip
      />
    ) : null;

  function parseMessage(text, id) {
    let lastIndex = -1;
    const result = [];
    const match = [...text.matchAll(finalReg)];

    if (match.length === 0) return text;

    match.forEach((str) => {
      if (str.index > lastIndex + 1) {
        result.push(str.input.substring(lastIndex, str.index));
      }

      const dataImg = str[1];
      const url = str[2];
      const native = str[3];
      const colons = str[4];
      const emoticons = str[5];

      if (dataImg) {
        result.push(<img src={dataImg} alt="Drawing" height="200" />);
      } else if (url) {
        const isImg = url.match(/\.(png|jpe?g|gif|webp)$/);
        let imgName = '';
        if (isImg) {
          const name = url.split('/');
          imgName = name[name.length - 1].replace(/\.(png|jpe?g|gif|webp)$/, '');
        }
        const res = (
          <>
            <a href={url} target="_blank" className="chat-room-messages__message-link" rel="noopener noreferrer">
              {url.length > 100 ? url.slice(0, 99) + '...' : url}
            </a>
            {isImg && <img src={url} alt={imgName.length > 30 ? 'Photo' : imgName} height="200" />}
          </>
        );
        result.push(res);
      } else if (native && data) {
        const nativeEmoji = getEmojiDataFromNative(native, 'apple', data);
        result.push(createEmoji(nativeEmoji.id, nativeEmoji.colons));
      } else if (colons) {
        const id = colons.slice(1, colons.length - 1);
        result.push(createEmoji(id, colons));
      } else if (emoticons) {
        const emoticonEmoji = emojiIndex.current?.search(emoticons)[0];
        result.push(emoticonEmoji ? createEmoji(emoticonEmoji.id, emoticons) : emoticons);
      }

      lastIndex = str.index + str[0].length;
    });

    if (lastIndex < text.length) {
      result.push(text.slice(lastIndex));
    }

    return result.map((el, i) => (typeof el === 'string' ? el : <Fragment key={`${id}${i}`}>{el}</Fragment>));
  }

  useEffect(() => {
    function updateComponent() {
      setDate(Date.now());
    }

    const updateInt = setInterval(updateComponent, MINUTE);

    return () => {
      clearInterval(updateInt);
    };
  }, []);

  const result = chats.map((message) => {
    const isYou = message.username === username;
    let onUsersClick;

    if (!isYou) {
      onUsersClick = () => handleUsersClick(message.username);
    }

    return (
      <div
        className={classNames('chat-room-messages__message-cont', {
          'align-right': isYou,
        })}
        key={message.id}
      >
        <span className="chat-room-messages__name" {...(!isYou && { onClick: onUsersClick })}>
          {message.username}
        </span>
        <span className="chat-room-messages__time">{timeDifference(message.date)}</span>
        <p className="chat-room-messages__message">{parseMessage(message.message, message.id)}</p>
      </div>
    );
  });

  return <>{result}</>;
});

ChatList.propTypes = {
  chats: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
  username: PropTypes.string.isRequired,
  emojiIndex: PropTypes.object,
  data: PropTypes.object,
  handleUsersClick: PropTypes.func.isRequired,
};

ChatList.displayName = 'ChatList';

export default ChatList;
