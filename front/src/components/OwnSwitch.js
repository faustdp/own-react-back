import { cloneElement, Children, isValidElement, useState, useEffect, useRef, useCallback } from 'react';
import { matchPath, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import { SwitchContext } from './SwitchContext';

const OwnSwitch = ({ children }) => {
  const [childs, setChilds] = useState([]);
  const { pathname } = useLocation();
  const delay = useRef({});
  const prevPath = useRef('');
  const currPath = useRef('');

  const setDelay = useCallback((path, time) => (delay.current[path] = time), []);

  const removeChild = useCallback((path) => {
    if (path === currPath.current) return;
    setChilds((prevState) => {
      if (prevState.length === 1) return prevState;
      const newChilds = [...prevState];
      newChilds.forEach((el, ind) => {
        if (el.props.path === path) newChilds.splice(ind, 1);
      });
      return newChilds;
    });
  }, []);

  const findMatch = useCallback((pathname, children) => {
    let match, child;
    Children.forEach(children, (element) => {
      if (match == null && isValidElement(element)) {
        const { path: pathProp, exact, strict, sensitive, from } = element.props;
        const path = pathProp || from;
        child = element;
        match = matchPath(pathname, { path, exact, strict, sensitive });
      }
    });
    return child;
  }, []);

  const renderChild = useCallback(() => {
    const child = findMatch(pathname, children);
    const newChilds = [];

    if (prevPath.current !== '/' && pathname !== '/') {
      newChilds.push(...childs);
      if (newChilds[0] && newChilds[0].props.to) {
        newChilds.splice(0, 1);
      }
    }

    if (child) {
      const { path } = child.props;
      if (!newChilds[0] || newChilds[0].props.path !== path || (newChilds[1] && newChilds[1].props.path !== path)) {
        if (newChilds.length > 1) {
          newChilds.shift();
        }
        newChilds.push(
          cloneElement(child, {
            key: child.props.path || 'redirect',
            removeChild,
            setDelay,
          }),
        );
      }
    }

    setChilds(newChilds);
  }, [childs, pathname, children, findMatch, removeChild, setDelay]);

  useEffect(() => {
    if (currPath.current !== pathname) {
      currPath.current = pathname;
      renderChild();
    }
  }, [pathname, renderChild]);

  useEffect(() => {
    prevPath.current = pathname;
  }, [childs]); // eslint-disable-line react-hooks/exhaustive-deps

  const value = {
    delay: delay.current,
    prevPath: prevPath.current,
  };

  return (
    <SwitchContext.Provider value={value}>
      <main className="cont">{childs}</main>
    </SwitchContext.Provider>
  );
};

OwnSwitch.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element]).isRequired,
};

export default OwnSwitch;
