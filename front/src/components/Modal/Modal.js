import { useState, useEffect, useRef, useCallback } from 'react';
import { createPortal } from 'react-dom';
import { useSelector, useDispatch } from 'react-redux';
import classNames from 'classnames';
import { toggleModal } from '../../redux/modalReducer';
import { usePrevious } from '../../helpers/hooks';

const root = document.getElementById('root');

function Modal({ children }) {
  const [ready, setReady] = useState(false);
  const contRef = useRef(null);
  const visibility = useSelector((state) => state.modal.visibility);
  const dispatch = useDispatch();
  const prevReady = usePrevious(ready);

  const closeModal = useCallback(() => {
    if (!ready) return;
    setReady(false);
  }, [ready]);

  const handleEscClose = useCallback(
    (e) => {
      if (ready && e.which === 27) closeModal();
    },
    [closeModal, ready],
  );

  function handleClick(e) {
    if (e.target === contRef.current) closeModal();
  }

  useEffect(() => {
    let updateTO = null;

    if (visibility) {
      updateTO = setTimeout(() => {
        setReady(true);
      }, 0);
    }

    if (!visibility) {
      setReady(false);
    }

    return () => {
      clearTimeout(updateTO);
    };
  }, [visibility]);

  useEffect(() => {
    document.addEventListener('keydown', handleEscClose);
    return () => {
      document.removeEventListener('keydown', handleEscClose);
    };
  }, [handleEscClose]);

  useEffect(() => {
    let closeTO = null;
    if (!ready && prevReady) {
      document.removeEventListener('keydown', handleEscClose);
      if (visibility) {
        closeTO = setTimeout(() => {
          dispatch(toggleModal({ visibility: false }));
        }, 410);
      }
    }

    return () => {
      clearTimeout(closeTO);
    };
  }, [ready, prevReady, visibility, dispatch, handleEscClose]);

  if (visibility === false && !ready) return null;

  return createPortal(
    <div className={classNames('modal', { 'is-visible': ready })} onClick={handleClick} ref={contRef}>
      <div className={classNames('modal-dialog', { 'is-visible': ready })}>{children}</div>
    </div>,
    root,
  );
}

export default Modal;
