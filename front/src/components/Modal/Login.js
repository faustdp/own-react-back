import { useState, useEffect, useRef, useCallback } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import classNames from 'classnames';
import { toggleModal, toggleSignUp } from '../../redux/modalReducer';
import { setUser } from '../../redux/userReducer';
import { LOGIN_MUTATION, SIGNUP_MUTATION, ADD_EVENT_MUTATION } from '../../api/queries';
import { useMutation } from '@apollo/client';

function Login() {
  const [error, setError] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const inputRef = useRef(null);
  const signUp = useSelector((state) => state.modal.signUp);
  const events = useSelector((state) => state.user.events, shallowEqual);
  const dispatch = useDispatch();
  const [loginMutation] = useMutation(LOGIN_MUTATION, { ignoreResults: true });
  const [signUpMutation] = useMutation(SIGNUP_MUTATION, { ignoreResults: true });
  const [addEventMutation] = useMutation(ADD_EVENT_MUTATION, { ignoreResults: true });

  const addEventsToDB = useCallback(() => {
    addEventMutation({
      variables: {
        events,
      },
    }).catch((err) => console.log(err));
  }, [addEventMutation, events]);

  const handleSignUpToggle = useCallback(
    (_, isLogin) => {
      if (!isLogin && !signUp) {
        dispatch(toggleSignUp(true));
        inputRef.current.focus();
      } else if (isLogin && signUp) {
        dispatch(toggleSignUp(false));
        inputRef.current.focus();
      }
    },
    [dispatch, signUp],
  );

  const handleLogInToggle = useCallback(() => {
    handleSignUpToggle(null, true);
  }, [handleSignUpToggle]);

  const handleSubmit = useCallback(
    (e) => {
      e.preventDefault();
      const data = new FormData(e.target);
      const username = data.get('username').trim();
      const password = data.get('password').trim();
      if (username.length < 2) {
        return setError('Username should be at least 2 characters long');
      }
      if (password.length < 4) {
        return setError('Password should be at least 4 characters long');
      }
      const mutation = signUp ? signUpMutation : loginMutation;
      mutation({
        variables: {
          username,
          password,
        },
      })
        .then(({ data }) => {
          const name = data?.logIn?.username || data?.signUp;
          if (events.length) addEventsToDB();
          dispatch(
            setUser({
              username: name,
              logged: true,
              ...(data?.logIn?.rooms && {
                rooms: data.logIn.rooms.map(({ id, room, participant }) => ({
                  id,
                  ...(room ? { room } : { participant }),
                })),
              }),
              ...(data?.logIn?.rooms && {
                unreadRooms: data.logIn.rooms.reduce((acc, curr) => {
                  if (curr.unreadCount != null) {
                    acc[curr.id] = curr.unreadCount;
                  }
                  return acc;
                }, {}),
              }),
            }),
          );
          dispatch(toggleModal({ visibility: false }));
        })
        .catch(({ message }) => {
          const index = message.lastIndexOf(': ');
          const error = index === -1 ? message : message.slice(index + 2);
          setError(error);
        });
    },
    [signUpMutation, loginMutation, signUp, events, addEventsToDB, dispatch],
  );

  function handleInputChange(e) {
    e.target.name === 'username' ? setUsername(e.target.value) : setPassword(e.target.value);
  }

  function handleBtnClick(e) {
    e.target.blur();
  }

  useEffect(() => {
    let errorTO = null;

    if (error !== '') {
      errorTO = setTimeout(() => setError(''), 2200);
    }
    return () => {
      clearTimeout(errorTO);
    };
  }, [error]);

  useEffect(() => {
    inputRef.current?.focus();
  }, []);

  return (
    <>
      <button className={classNames('modal-btn', { 'is-active': signUp })} onClick={handleSignUpToggle}>
        Sign Up
      </button>
      <button className={classNames('modal-btn', { 'is-active': !signUp })} onClick={handleLogInToggle}>
        Log In
      </button>
      <p className="modal-title">{signUp ? 'Sign Up for Free' : 'Log In'}</p>
      <form className="modal-form" onSubmit={handleSubmit}>
        <input
          type="text"
          name="username"
          className="modal-input"
          placeholder="Username"
          autoComplete="username"
          onChange={handleInputChange}
          value={username}
          ref={inputRef}
        />
        <input
          type="password"
          name="password"
          className="modal-input"
          placeholder="Password"
          autoComplete="current-password"
          onChange={handleInputChange}
          value={password}
        />
        {error && <p className="modal-error">{error}.</p>}
        <button className="modal-btn modal-btn--big is-active" onClick={handleBtnClick}>
          {signUp ? 'REGISTER' : 'WELCOME BACK'}
        </button>
      </form>
    </>
  );
}

export default Login;
