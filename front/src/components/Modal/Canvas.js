import { useState, useEffect, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import throttle from '../../helpers/throttle';

function Canvas({ canvasDrawing, onSubmit }) {
  const [state, stateSet] = useState({
    actions: canvasDrawing.current.length > 0 ? [...canvasDrawing.current] : [],
    index: canvasDrawing.current.length > 0 ? canvasDrawing.current.length - 1 : 0,
  });
  const canvasRef = useRef();
  const context = useRef();
  const isDrawing = useRef(false);
  const coordsDiff = useRef({ x: 1, y: 1, ratio: window?.devicePixelRatio });

  function getCoords(e) {
    return {
      x: e.changedTouches ? e.changedTouches[0].pageX - canvasRef.current.getBoundingClientRect().x : e.offsetX,
      y: e.changedTouches ? e.changedTouches[0].pageY - canvasRef.current.getBoundingClientRect().y : e.offsetY,
    };
  }

  const clearCanvas = useCallback(() => {
    context.current.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);
    stateSet({ actions: [], index: 0 });
    canvasDrawing.current.splice(0, canvasDrawing.current.length);
  }, [canvasDrawing]);

  const stopDraw = useCallback(() => {
    if (!isDrawing.current) return;
    const url = canvasRef.current.toDataURL('image/webp');
    canvasDrawing.current.push(url);
    stateSet((st) => {
      if (st.index < st.actions.length - 1) {
        const res = st.actions.slice(0, st.index + 1);
        res.push(url);
        return {
          actions: res,
          index: st.index + 1,
        };
      } else {
        return {
          actions: [...st.actions, url],
          index: st.actions.length,
        };
      }
    });
    isDrawing.current = false;
  }, [canvasDrawing]);

  const draw = useCallback((e) => {
    e.preventDefault();
    if ((e.type === 'mousemove' && e.which !== 1) || (e.type === 'touchmove' && e.changedTouches.length > 1)) return;
    isDrawing.current = true;
    const { x, y } = getCoords(e);
    context.current.lineTo(x * coordsDiff.current.x, y * coordsDiff.current.y);
    context.current.stroke();
  }, []);

  const setPosition = useCallback((e, draw) => {
    if (e.type === 'touchstart' && e.changedTouches.length > 1) return;
    const { x, y } = getCoords(e);
    context.current.beginPath();
    context.current.moveTo(x * coordsDiff.current.x, y * coordsDiff.current.y);
    if (draw && (e.which === 1 || e.touches)) {
      isDrawing.current = true;
      context.current.fillRect((x - 2) * coordsDiff.current.x, (y - 2) * coordsDiff.current.y, 4, 4);
    }
  }, []);

  const undoCanvas = useCallback(
    (_, redo) => {
      const width = canvasRef.current.width;
      const height = canvasRef.current.height;
      const diff = state.index + (redo ? 1 : -1);
      if (diff < 0) {
        context.current.clearRect(0, 0, width / coordsDiff.current.ratio, height / coordsDiff.current.ratio);
        canvasDrawing.current.splice(0, canvasDrawing.current.length);
      } else {
        const img = new Image();
        img.src = state.actions[diff];
        img.onload = function () {
          context.current.clearRect(0, 0, width / coordsDiff.current.ratio, height / coordsDiff.current.ratio);
          context.current.drawImage(img, 0, 0, width / coordsDiff.current.ratio, height / coordsDiff.current.ratio);
          redo ? canvasDrawing.current.push(state.actions[diff]) : canvasDrawing.current.splice(diff + 1, 1);
        };
      }
      stateSet((st) => ({
        ...st,
        index: diff,
      }));
    },
    [state, canvasDrawing],
  );

  function submitCanvas() {
    onSubmit();
  }

  const initCanvas = useCallback(() => {
    const { offsetWidth, offsetHeight } = canvasRef.current;
    const width = offsetWidth * coordsDiff.current.ratio;
    const height = offsetHeight * coordsDiff.current.ratio;
    const radius = 15;
    canvasRef.current.width = width;
    canvasRef.current.height = height;
    context.current.scale(coordsDiff.current.ratio, coordsDiff.current.ratio);
    context.current.lineWidth = 4;
    context.current.lineCap = 'round';
    context.current.fillStyle = '#bbb';
    context.current.moveTo(radius, 0);
    context.current.arcTo(width, 0, width, height, radius);
    context.current.arcTo(width, height, 0, height, radius);
    context.current.arcTo(0, height, 0, 0, radius);
    context.current.arcTo(0, 0, width, 0, radius);
    context.current.closePath();
    context.current.fill();
    context.current.strokeStyle = '#068006';
    context.current.fillStyle = '#068006';
    if (canvasDrawing.current.length > 0) {
      const img = new Image();
      img.src = canvasDrawing.current[canvasDrawing.current.length - 1];
      img.onload = function () {
        context.current.clearRect(0, 0, width / coordsDiff.current.ratio, height / coordsDiff.current.ratio);
        context.current.drawImage(img, 0, 0, width / coordsDiff.current.ratio, height / coordsDiff.current.ratio);
      };
    }
  }, [canvasDrawing]);

  const resizeCanvas = useCallback(() => {
    coordsDiff.current.x = canvasRef.current.width / (canvasRef.current.offsetWidth * coordsDiff.current.ratio);
    coordsDiff.current.y = canvasRef.current.height / (canvasRef.current.offsetHeight * coordsDiff.current.ratio);
  }, []);

  useEffect(() => {
    if (!canvasRef.current) return;
    const canvas = canvasRef.current;
    context.current = canvas.getContext('2d');

    initCanvas();

    const throttledResize = throttle(resizeCanvas, 100);

    function mousedownListener(e) {
      e.preventDefault();
      setPosition(e, true);
    }

    function focusListener(e) {
      if (canvas.nextElementSibling.children.length > 0) {
        e.preventDefault();
        canvas.nextElementSibling.children[0].focus();
      }
    }

    canvas.addEventListener('mouseenter', setPosition);
    canvas.addEventListener('mousedown', mousedownListener);
    canvas.addEventListener('touchstart', mousedownListener, { passive: false });
    canvas.addEventListener('mousemove', draw);
    canvas.addEventListener('touchmove', draw, { passive: false });
    canvas.addEventListener('mouseout', stopDraw);
    canvas.addEventListener('mouseup', stopDraw);
    canvas.addEventListener('touchend', stopDraw);
    window.addEventListener('focus', focusListener, { once: true, capture: true });
    window.addEventListener('resize', throttledResize);

    return () => {
      canvas.removeEventListener('mouseenter', setPosition);
      canvas.removeEventListener('mousedown', mousedownListener);
      canvas.removeEventListener('touchstart', mousedownListener);
      canvas.removeEventListener('mousemove', draw);
      canvas.removeEventListener('touchmove', draw);
      canvas.removeEventListener('mouseout', stopDraw);
      canvas.removeEventListener('mouseup', stopDraw);
      canvas.removeEventListener('touchend', stopDraw);
      window.removeEventListener('focus', focusListener, { capture: true });
      window.removeEventListener('resize', throttledResize);
    };
  }, [initCanvas, resizeCanvas, draw, setPosition, stopDraw]);

  const len = state.actions.length;

  return (
    <div className="modal-canvas__cont">
      <canvas className="modal-canvas" ref={canvasRef}></canvas>
      <div className="modal-canvas__buttons">
        {len > 0 && (
          <button className="modal-canvas__button" onClick={clearCanvas}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 492 492" width="20" height="26">
              <path
                style={{ fill: '#e21010' }}
                d="M300.188,246L484.14,62.04c5.06-5.064,7.852-11.82,7.86-19.024c0-7.208-2.792-13.972-7.86-19.028
                L468.02,7.872c-5.068-5.076-11.824-7.856-19.036-7.856c-7.2,0-13.956,2.78-19.024,7.856L246.008,
                191.82L62.048,7.872c-5.06-5.076-11.82-7.856-19.028-7.856c-7.2,0-13.96,2.78-19.02,7.856L7.872,
                23.988c-10.496,10.496-10.496,27.568,0,38.052L191.828,246L7.872,429.952c-5.064,5.072-7.852,
                11.828-7.852,19.032c0,7.204,2.788,13.96,7.852,19.028l16.124,16.116c5.06,5.072,11.824,7.856,
                19.02,7.856c7.208,0,13.968-2.784,19.028-7.856l183.96-183.952l183.952,183.952c5.068,5.072,11.824,
                7.856,19.024,7.856h0.008c7.204,0,13.96-2.784,19.028-7.856l16.12-16.116c5.06-5.064,7.852-11.824,
                7.852-19.028c0-7.204-2.792-13.96-7.852-19.028L300.188,246z"
              />
            </svg>
          </button>
        )}
        {len > 0 && state.index > 0 && (
          <button className="modal-canvas__button" onClick={undoCanvas}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 472.615 472.615" width="26" height="26">
              <polygon
                style={{ fill: '#ddef4b' }}
                points="205.783,139.662 205.783,30.525 0,236.308 205.783,442.09 205.783,332.955 472.615,332.955
                472.615,139.662"
              />
            </svg>
          </button>
        )}
        {len > 0 && state.index < len - 1 && (
          <button className="modal-canvas__button" onClick={(e) => undoCanvas(e, true)}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 491.807 491.807" width="26" height="26">
              <path
                style={{ fill: '#ddef4b' }}
                d="M485.802,221.184l-184.32-184.32c-5.857-5.837-14.643-7.619-22.323-4.444c-7.659,3.154-12.636,
                10.65-12.636,18.924v82.002c-117.842,4.833-220.651,84.398-253.583,198.41c-14.254,49.439-13.148,
                89.6-12.739,104.735l0.082,3.973c0,9.216,6.164,17.306,15.032,19.743c1.823,0.492,3.645,0.737,5.448,
                0.737c7.086,0,13.844-3.707,17.592-9.994c81.121-136.356,188.6-140.227,228.168-136.376v105.411c0,8.274,
                4.977,15.77,12.636,18.924c7.68,3.195,16.466,1.413,22.323-4.444l184.32-184.32C493.809,242.135,493.809,
                229.191,485.802,221.184z"
              />
            </svg>
          </button>
        )}
        {len > 0 && (
          <button className="modal-canvas__button" onClick={submitCanvas}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.837 17.837" width="26" height="26">
              <path
                style={{ fill: '#00d882' }}
                d="M16.145,2.571c-0.272-0.273-0.718-0.273-0.99,0L6.92,10.804l-4.241-4.27c-0.272-0.274-0.715-0.274-0.989
                ,0L0.204,8.019c-0.272,0.271-0.272,0.717,0,0.99l6.217,6.258c0.272,0.271,0.715,0.271,0.99,0
                L17.63,5.047c0.276-0.273,0.276-0.72,0-0.994L16.145,2.571z"
              />
            </svg>
          </button>
        )}
      </div>
    </div>
  );
}

Canvas.propTypes = {
  canvasDrawing: PropTypes.object,
  onSubmit: PropTypes.func,
};

export default Canvas;
