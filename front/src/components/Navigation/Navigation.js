import { NavLink, useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { toggleModal } from '../../redux/modalReducer';
import { logoutUser, setUser } from '../../redux/userReducer';
import LoadableChat from '../Chat';
import LoadableCalendar from '../Calendar';
import LoadableHome from '../Home';
import { useMutation } from '@apollo/client';
import { LOGOUT_MUTATION } from '../../api/queries';
import { subscriptionClient } from '../../api/client';
import Modal from '../Modal/Modal';
import Login from '../Modal/Login';

function Navigation() {
  const logged = useSelector((state) => state.user.logged);
  const canvas = useSelector((state) => state.modal.canvas);
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const [logOutMutation] = useMutation(LOGOUT_MUTATION, { ignoreResults: true });

  async function onLogout(e) {
    e.target.blur();
    try {
      const {
        data: {
          logOut: { username, rooms, isLogged },
        },
      } = await logOutMutation();
      dispatch(logoutUser());
      subscriptionClient.dispose();
      const filteredRooms = rooms.map(
        ({ __typename, ...rest }) => (Object.keys(rest).forEach((el) => rest[el] === null && delete rest[el]), rest),
      );
      const unreadRooms = rooms.reduce((acc, curr) => {
        if (curr.unreadCount != null) {
          acc[curr.id] = curr.unreadCount;
        }
        return acc;
      }, {});
      await new Promise((resolve) => setTimeout(resolve, 5));
      dispatch(setUser({ username, logged: isLogged, rooms: filteredRooms, unreadRooms }));
    } catch (err) {
      console.log(err);
    }
  }

  function handleClick(e) {
    e.target.blur();
    dispatch(toggleModal({ visibility: true, canvas: false }));
  }

  function handleMouseOver(e) {
    const link = e.target.getAttribute('href');
    switch (link) {
      case '/':
        LoadableHome.preload();
        break;
      case '/chat':
        LoadableChat.preload();
        break;
      case '/calendar':
        LoadableCalendar.preload();
        break;
    }
  }

  return (
    <nav className="nav">
      {pathname !== '/' && (
        <NavLink to="/" exact className="nav__link" onMouseOver={handleMouseOver}>
          Home
        </NavLink>
      )}
      <NavLink to="/chat" activeClassName="nav__link--active" className="nav__link" onMouseOver={handleMouseOver}>
        Chat
      </NavLink>
      <NavLink
        to="/calendar"
        exact
        activeClassName="nav__link--active"
        className="nav__link"
        onMouseOver={handleMouseOver}
      >
        Calendar
      </NavLink>
      {pathname !== '/' && !logged && (
        <button className="nav__link" onClick={handleClick}>
          Sign In
        </button>
      )}
      {pathname !== '/' && !logged && !canvas && (
        <Modal>
          <Login />
        </Modal>
      )}
      {logged && (
        <button className="nav__link" onClick={onLogout}>
          Logout
        </button>
      )}
    </nav>
  );
}

export default Navigation;
