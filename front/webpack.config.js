const webpack = require('webpack');
const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const autoprefixer = require('autoprefixer');
const flexbugs = require('postcss-flexbugs-fixes');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const cssnano = require('cssnano');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const zlib = require('zlib');
const Dotenv = require('dotenv-webpack');
const { GenerateSW } = require('workbox-webpack-plugin');

const isDev = process.env.NODE_ENV !== 'production';

const src = resolve(__dirname, 'src');

const cfg = {
  mode: isDev ? 'development' : 'production',
  devtool: isDev ? 'eval-cheap-module-source-map' : false,
  entry: {
    bundle: './src/index.js',
  },
  output: {
    path: resolve(__dirname, 'build'),
    publicPath: '/',
    filename: isDev ? 'js/[name].js' : 'js/[name].[chunkhash:10].js',
    chunkFilename: isDev ? 'js/[name].js' : 'js/[name].[chunkhash:10].js',
    crossOriginLoading: isDev ? 'use-credentials' : false,
    assetModuleFilename: 'assets/[name].[hash][ext]',
  },
  module: {
    rules: [
      {
        test: /\.(mjs|js|jsx)$/,
        include: src,
        exclude: /node_modules\/(?!emoji-mart)\/.*/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
              plugins: [isDev && require.resolve('react-refresh/babel')].filter(Boolean),
            },
          },
        ],
      },
      {
        test: /\.(scss)$/,
        use: [
          ...(isDev
            ? [{ loader: 'style-loader', options: { injectType: 'singletonStyleTag' } }]
            : [{ loader: MiniCssExtractPlugin.loader }]),
          { loader: 'css-loader', options: { sourceMap: true } },
          { loader: 'resolve-url-loader', options: { sourceMap: true } },
          ...(!isDev
            ? [
                {
                  loader: 'postcss-loader',
                  options: { sourceMap: true, postcssOptions: { plugins: [autoprefixer, flexbugs, cssnano] } },
                },
              ]
            : []),
          { loader: 'sass-loader', options: { sourceMap: true, implementation: require('sass') } },
        ],
      },
      {
        test: /\.(svg|png|jpe?g|gif|woff|woff2|ico)$/,
        type: 'asset',
      },
    ],
  },
  devServer: {
    static: './public',
    client: {
      logging: 'warn',
    },
    proxy: {
      '/graphql': 'http://localhost:8000',
    },
  },
  resolve: {
    modules: [src, __dirname + '/src/components', 'node_modules'],
    extensions: ['.mjs', '.js', '.jsx'],
  },
  optimization: {
    ...(!isDev && {
      splitChunks: {
        minChunks: 2,
        name(_, chunks) {
          const names = chunks.map((item) => item.name).join('~');
          return `common~${names}`;
        },
        cacheGroups: {
          defaultVendors: {
            test: /node_modules/,
            name: 'vendor',
            chunks: 'initial',
            enforce: true,
          },
          styles: {
            name: 'styles',
            type: 'css/mini-extract',
            chunks: 'all',
            enforce: true,
          },
        },
      },
      minimize: true,
      minimizer: [
        new TerserPlugin({
          terserOptions: {
            ecma: 8,
            compress: {
              warnings: false,
              comparisons: false,
            },
            output: {
              comments: false,
            },
          },
        }),
      ],
    }),
  },
  performance: {
    hints: false,
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: './public/index.html',
      minify: {
        removeComments: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
      },
    }),
    ...(isDev
      ? [new ReactRefreshWebpackPlugin()]
      : [
          new Dotenv({ systemvars: true }),
          new CleanWebpackPlugin(),
          new MiniCssExtractPlugin({ filename: 'css/styles.[contenthash:10].css' }),
          new webpack.SourceMapDevToolPlugin({
            filename: 'js/[name].map',
            exclude: [/vendor\..+\.(mjs|js|jsx)$/, /common.+\.(mjs|js|jsx)$/],
          }),
          new CopyWebpackPlugin({
            patterns: [
              {
                from: './public/*.(ico|json|webp|png|jpg)',
                to: './[name][ext]',
              },
            ],
          }),
          new WebpackManifestPlugin({ fileName: 'asset-manifest.json' }),
          new CompressionPlugin({
            filename: '[path][base].br',
            algorithm: 'brotliCompress',
            test: /\.(js|css)$/,
            compressionOptions: {
              params: {
                [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
              },
            },
            minRatio: 0.8,
            deleteOriginalAssets: false,
          }),
          new GenerateSW({
            cleanupOutdatedCaches: true,
            navigateFallback: '/index.html',
            navigateFallbackAllowlist: [/chat/, /calendar/],
          }),
        ]),
  ],
};

module.exports = cfg;
